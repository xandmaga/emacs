(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes (quote (tango-dark)))
 '(shell-file-name "C:/Windows/SysWOW64/WindowsPowerShell/v1.0/powershell.exe")
 '(shell-mode-hook nil)
 '(w32-system-shells (quote ("C:/Windows/SysWOW64/WindowsPowerShell/v1.0/powershell.exe" "command" "command.com" "4nt" "4nt.exe" "4dos" "4dos.exe" "tcc" "tcc.exe" "ndos" "ndos.exe"))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                         ("marmalade" . "http://marmalade-repo.org/packages/")
                         ("melpa" . "http://melpa.milkbox.net/packages/")))

(let ((default-directory "~/.emacs.d/"))
  (normal-top-level-add-subdirs-to-load-path))


;; Must load packages
;; (require 'auto-compile)
;; (auto-compile-on-load-mode 1)
;; (auto-compile-on-save-mode 1)
(require 'cl)
(require 'dash)
(require 's)
(require 'f)
(require 'noflet)
(require 'bind-key)
(require 'smartrep)
(require 'use-package)
(require 'diminish)
;;(require 'cedet-devel-load nil t)
(require 'package)

(use-package auto-complete)
(use-package powershell)
(use-package powershell-mode)

