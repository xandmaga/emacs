﻿begin;

/* Atualizacao de jbpm_token */
update jbpm_token tk
set end_ = null, isabletoreactivateparent_ = true,node_ = (select tasknode_ from jbpm_task where id_ = opa.task_)
from
(select 
ti.token_,ti.task_
from jbpm_taskinstance ti inner join core.tb_processo_instance pi on pi.id_proc_inst =  ti.procinst_
inner join 
(select id_processo,max(end_) as end_ from jbpm_taskinstance ti inner join core.tb_processo_instance pi on pi.id_proc_inst =  ti.procinst_
and id_processo in(
select distinct id_processo_trf from client.tb_processo_trf ptf where cd_processo_status = 'D' and not exists 
(select 1 from client.vs_situacao_processo_new vs where vs.id_processo_trf = ptf.id_processo_trf))
and in_ativo = true
group by id_processo) as dados
on dados.id_processo = pi.id_processo and ti.end_ = dados.end_) as opa
where opa.token_ = tk.id_;

/* Atualização do apontamento do token pai */
update jbpm_token tk1
set node_ = opa.id_node_correto
from 
(select nd_correto.id_ as id_node_correto, tk_pai.id_ as id_token_pai from jbpm_processinstance pi 
inner join jbpm_processdefinition pd_filha on pd_filha.id_ = pi.processdefinition_
inner join jbpm_token tk on pi.roottoken_ = tk.id_
inner join jbpm_token tk_pai on tk_pai.id_ = pi.superprocesstoken_
inner join jbpm_node nd_pai on nd_pai.id_ = tk_pai.node_
inner join jbpm_node nd_correto on nd_pai.processdefinition_ = nd_correto.processdefinition_ and nd_correto.subprocname_ = pd_filha.name_
where nd_pai.subprocname_ is null and pi.id_ in (select 
ti.procinst_
from jbpm_taskinstance ti inner join core.tb_processo_instance pi on pi.id_proc_inst =  ti.procinst_
inner join 
(select id_processo,max(end_) as end_ from jbpm_taskinstance ti inner join core.tb_processo_instance pi on pi.id_proc_inst =  ti.procinst_
and id_processo in(
select distinct id_processo_trf from client.tb_processo_trf ptf where cd_processo_status = 'D' and not exists 
(select 1 from client.vs_situacao_processo_new vs where vs.id_processo_trf = ptf.id_processo_trf))
and in_ativo = true
group by id_processo) as dados
on dados.id_processo = pi.id_processo and ti.end_ = dados.end_)) as opa
where opa.id_token_pai = tk1.id_ ;


/* Atualizacao de jbpm_processinstance */
update jbpm_processinstance set end_ = null
where id_ in
(select 
ti.procinst_
from jbpm_taskinstance ti inner join core.tb_processo_instance pi on pi.id_proc_inst =  ti.procinst_
inner join 
(select id_processo,max(end_) as end_ from jbpm_taskinstance ti inner join core.tb_processo_instance pi on pi.id_proc_inst =  ti.procinst_
and id_processo in(
select distinct id_processo_trf from client.tb_processo_trf ptf where cd_processo_status = 'D' and not exists 
(select 1 from client.vs_situacao_processo_new vs where vs.id_processo_trf = ptf.id_processo_trf))
and in_ativo = true
group by id_processo) as dados
on dados.id_processo = pi.id_processo and ti.end_ = dados.end_);

/* Atualizacao de jbpm_taskinstance */
update jbpm_taskinstance set end_ = null,isopen_ = true,issignalling_ = true, issuspended_ = false 
where id_ in
(select 
ti.id_
from jbpm_taskinstance ti inner join core.tb_processo_instance pi on pi.id_proc_inst =  ti.procinst_
inner join 
(select id_processo,max(end_) as end_ from jbpm_taskinstance ti inner join core.tb_processo_instance pi on pi.id_proc_inst =  ti.procinst_
and id_processo in(
select distinct id_processo_trf from client.tb_processo_trf ptf where cd_processo_status = 'D' and not exists 
(select 1 from client.vs_situacao_processo_new vs where vs.id_processo_trf = ptf.id_processo_trf))
and in_ativo = true
group by id_processo) as dados
on dados.id_processo = pi.id_processo and ti.end_ = dados.end_);

commit;
