﻿CREATE OR REPLACE FUNCTION add_id_municipio_ibge() RETURNS void AS
$BODY$
BEGIN
IF NOT EXISTS
        (SELECT 1 FROM information_schema.columns        
                WHERE table_schema = 'core'
                AND table_name = 'tb_municipio'
                AND column_name = 'id_municipio_ibge')
THEN
        ALTER TABLE core.tb_municipio ADD COLUMN id_municipio_ibge varchar(10);
END IF;

END
$BODY$
LANGUAGE plpgsql VOLATILE STRICT;

SELECT add_id_municipio_ibge();

DROP FUNCTION add_id_municipio_ibge();


CREATE OR REPLACE FUNCTION add_nr_ano_eleicao() RETURNS void AS
$BODY$
BEGIN
IF EXISTS
        (SELECT 1 FROM information_schema.columns
                WHERE table_schema = 'client'
                AND table_name = 'tb_eleicao'
                AND column_name = 'nr_ano_aleicao')
THEN
        ALTER TABLE client.tb_eleicao DROP COLUMN nr_ano_aleicao;
END IF;

IF EXISTS
        (SELECT 1 FROM information_schema.columns
                WHERE table_schema = 'client'
                AND table_name = 'tb_eleicao'
                AND column_name = 'ds_ano_eleicao')
THEN
        ALTER TABLE client.tb_eleicao DROP COLUMN ds_ano_eleicao;
END IF;


IF NOT EXISTS
        (SELECT 1 FROM information_schema.columns
                WHERE table_schema = 'client'
                AND table_name = 'tb_eleicao'
                AND column_name = 'nr_ano_eleicao')
THEN
        ALTER TABLE client.tb_eleicao ADD COLUMN nr_ano_eleicao integer;
END IF;

END
$BODY$
LANGUAGE plpgsql VOLATILE STRICT;

SELECT add_nr_ano_eleicao();

DROP FUNCTION add_nr_ano_eleicao();


/**
 * Inclusão do campo id_instancia_origem em tb_processo_documento para guardar o id
 * da instancia de origem quando o documento for criado em outra instância e remetido.
 */
CREATE OR REPLACE FUNCTION add_id_instancia_origem() RETURNS void AS
$BODY$
BEGIN
IF NOT EXISTS
        (SELECT 1 FROM information_schema.columns        
                WHERE table_schema = 'core'
                AND table_name = 'tb_processo_documento'
                AND column_name = 'id_instancia_origem')
THEN
        ALTER TABLE core.tb_processo_documento ADD COLUMN id_instancia_origem core.id;
END IF;

END
$BODY$
LANGUAGE plpgsql VOLATILE STRICT;

SELECT add_id_instancia_origem();

DROP FUNCTION add_id_instancia_origem();

/**
 * [PJEII-19739]
 */

DO $$

BEGIN
        ALTER TABLE client.tb_pessoa_juridica
        ALTER COLUMN nm_fantasia
        DROP NOT NULL;        
END $$;


/**
 * [PJEII-19607]
 * Altera o processo documento de uma instância diferente, passando o valor do campo número para id_instancia_origem
 *
 */

CREATE OR REPLACE FUNCTION normalizaNumeroInstanciaOrigem()
RETURNS void AS $$
DECLARE
   processoDocumentos RECORD;
BEGIN        
        -- atualiza os processsosDocumentos com o nr_documento
           FOR processoDocumentos IN SELECT * FROM core.tb_processo_documento tpd
                WHERE exists (select 1 from core.tb_parametro p  where p.nm_variavel='aplicacaoSistema' and p.vl_variavel<>tpd.ds_instancia)
                and tpd.nr_documento is not null
           LOOP
                BEGIN
                      update core.tb_processo_documento
                      set id_instancia_origem=nr_documento::INTEGER
                      WHERE
                      id_processo_documento = processoDocumentos.id_processo_documento;
                      RAISE NOTICE 'Documento atualizado. ProcessoDocumento "%"; Numero do documento "%"',processoDocumentos.id_processo_documento, processoDocumentos.nr_documento;
                EXCEPTION WHEN OTHERS THEN
                        RAISE NOTICE 'Número documento inválido: "%".', processoDocumentos.nr_documento;
                END;
           END LOOP;
    
RETURN ;
END;
$$ LANGUAGE plpgsql;


select normalizaNumeroInstanciaOrigem();


drop function normalizaNumeroInstanciaOrigem();
