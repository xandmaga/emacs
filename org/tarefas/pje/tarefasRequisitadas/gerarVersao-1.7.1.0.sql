/*
 * Ronaldo Ferreira de Almeida
 * [PJEII-18836]
 */

DO $$

BEGIN
	alter table client.tb_caixa_adv_proc alter column dt_distribuicao_inicio type client.data_hora;
	alter table client.tb_caixa_adv_proc alter column dt_distribuicao_fim type client.data_hora;
	alter table client.tb_caixa_adv_proc alter column dt_ano_nasc_parte_inicio type client.data_hora;
	alter table client.tb_caixa_adv_proc alter column dt_ano_nasc_parte_fim type client.data_hora;	
END $$;
/*
 * Alexander da Costa Monteiro
 * [PJEII-18814]
 */

DO $$

BEGIN

ALTER TABLE client.tb_pess_proc_jurisdicao DROP CONSTRAINT IF EXISTS tb_pess_proc_jurisdicao_id_pessoa_procuradoria_fkey;

ALTER TABLE client.tb_pess_proc_jurisdicao
  ADD CONSTRAINT tb_pess_proc_jurisdicao_id_pessoa_procuradoria_fkey FOREIGN KEY (id_pessoa_procuradoria)
      REFERENCES client.tb_pessoa_procuradoria (id_pessoa_procuradoria) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE;
      
END $$;
/*
 * Rodrigo Santos Menezes
 * [PJEII-18813] Criação de tabela associativa entre ProcessoParteExpediente e CaixaAdvogadoProcurador
 */

DO $$

BEGIN
	IF NOT EXISTS (SELECT 1 FROM information_schema.sequences WHERE sequence_schema = 'client' AND sequence_name = 'sq_tb_proc_parte_exp_caixa_adv_proc') THEN
		CREATE SEQUENCE client.sq_tb_proc_parte_exp_caixa_adv_proc INCREMENT 1 MINVALUE 1 START 1 CACHE 1;
	END IF;
	
	IF NOT EXISTS
		(SELECT 1 FROM information_schema.columns	
			WHERE table_schema = 'client' 
			AND table_name = 'tb_proc_parte_exp_caixa_adv_proc' )
	
	THEN	
		CREATE TABLE client.tb_proc_parte_exp_caixa_adv_proc
		(
		  id_proc_parte_exp_caixa_adv_proc integer NOT NULL DEFAULT nextval('client.sq_tb_proc_parte_exp_caixa_adv_proc'::regclass),
		  id_processo_parte_expediente integer NOT NULL,
		  id_caixa_adv_proc integer NOT NULL,
		  CONSTRAINT tb_proc_parte_exp_caixa_adv_proc_pkey PRIMARY KEY (id_proc_parte_exp_caixa_adv_proc),
		  CONSTRAINT tb_ppe_caixa_adv_proc_id_caixa_adv_proc_fkey FOREIGN KEY (id_caixa_adv_proc)
		      REFERENCES client.tb_caixa_adv_proc (id_caixa_adv_proc) MATCH SIMPLE
		      ON UPDATE NO ACTION ON DELETE NO ACTION,
		  CONSTRAINT tb_ppe_caixa_adv_proc_id_processo_parte_expediente_fkey FOREIGN KEY (id_processo_parte_expediente)
		      REFERENCES client.tb_proc_parte_expediente (id_processo_parte_expediente) MATCH SIMPLE
		      ON UPDATE NO ACTION ON DELETE NO ACTION
		)
	WITH (
	  OIDS=FALSE	
	);
	
END IF;

END $$;	
/*
 * Rodrigo Santos Menezes
 * [PJEII-18813]
 */

DO $$

BEGIN
IF NOT EXISTS
	(SELECT 1 FROM information_schema.columns	
		WHERE table_schema = 'client' 
		AND table_name = 'tb_log_hist_movimentacao' 
		AND column_name = 'id_processo_parte_expediente')
THEN

	ALTER TABLE client.tb_log_hist_movimentacao ADD COLUMN id_processo_parte_expediente integer;
	ALTER TABLE client.tb_log_hist_movimentacao ADD CONSTRAINT tb_log_hist_mov_id_processo_parte_expediente_fkey FOREIGN KEY (id_processo_parte_expediente)
    REFERENCES client.tb_proc_parte_expediente (id_processo_parte_expediente) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;
	COMMENT ON COLUMN client.tb_log_hist_movimentacao.id_processo_parte_expediente IS 'Identificador do processo parte expediente';

END IF;

END $$;
/*
 * Rodrigo Santos Menezes
 * [PJEII-18813]
 */

DO $$

BEGIN
IF NOT EXISTS
	(SELECT 1 FROM information_schema.columns	
		WHERE table_schema = 'client' 
		AND table_name = 'tb_log_hist_movimentacao' 
		AND column_name = 'nm_caixa_adv_proc')
THEN

	ALTER TABLE client.tb_log_hist_movimentacao ADD COLUMN nm_caixa_adv_proc client.varchar100;

END IF;

END $$;
/*
 * Thiago de Andrade Vieira
 * [PJEII-19261]
 */

update core.tb_processo_documento pd set ds_nome_usuario_alteracao = (select ds_nome from acl.tb_usuario_login where id_usuario = pd.id_usuario_alteracao)
where dt_juntada is not null and id_usuario_alteracao is not null and ds_nome_usuario_alteracao is null;
 
update core.tb_processo_documento pd set ds_nome_usuario_inclusao = (select ds_nome from acl.tb_usuario_login where id_usuario = pd.id_usuario_inclusao)
where dt_juntada is not null and id_usuario_inclusao is not null and ds_nome_usuario_inclusao is null;
/*
 * Thiago de Andrade Vieira
 * [PJEII-19261]
 */

CREATE OR REPLACE VIEW client.vs_destinatario_expediente AS
(
	SELECT ppe.id_processo_parte_expediente, ppe.id_pessoa_parte as id_destinatario
	FROM client.tb_proc_parte_expediente ppe
	INNER JOIN client.tb_processo_expediente pe ON ppe.id_processo_expediente = pe.id_processo_expediente
	UNION ALL
	SELECT ppe.id_processo_parte_expediente, ppr.id_representante as id_destinatario
	FROM client.tb_proc_parte_expediente ppe
	INNER JOIN client.tb_processo_trf prt ON ppe.id_processo_trf = prt.id_processo_trf
	INNER JOIN client.tb_processo_parte pp ON prt.id_processo_trf = pp.id_processo_trf and pp.id_pessoa = ppe.id_pessoa_parte
	INNER JOIN client.tb_processo_expediente pe ON ppe.id_processo_expediente = pe.id_processo_expediente
	INNER JOIN client.tb_proc_parte_represntante ppr ON pp.id_processo_parte = ppr.id_processo_parte
);
/*
 * Augusto Martins
 * Criação da funcionalidade Defensoria e atribuição de acesso aos mesmos perfis que acessam
 * a tela de Procuradoria
 */

DO $$

BEGIN
IF NOT EXISTS

	(SELECT 1 FROM acl.tb_papel t_papel
		WHERE t_papel.ds_identificador = '/pages/Defensoria/listView.seam')

THEN

	INSERT INTO acl.tb_papel( id_papel, in_condicional, ds_identificador, ds_nome, in_ativo)
	VALUES (nextval('acl.sq_tb_papel'), false, '/pages/Defensoria/listView.seam', 'Defensoria', true);

END IF;

END $$;


DO $$

BEGIN
IF NOT EXISTS

	(SELECT 1 FROM acl.tb_papel t_papel
		WHERE t_papel.ds_identificador = '/pages/Defensoria/listView.seam')

THEN

	INSERT INTO acl.tb_papel( id_papel, in_condicional, ds_identificador, ds_nome, in_ativo)
	VALUES (nextval('acl.sq_tb_papel'), false, '/pages/Defensoria/listView.seam', 'Defensoria', true);

END IF;

END $$;

DO $$

BEGIN
IF NOT EXISTS

	(select 1 from acl.tb_papel_grupo where membro_do_grupo = (select id_papel from acl.tb_papel where ds_identificador = '/pages/Procuradoria/listView.seam'))

THEN

INSERT INTO acl.tb_papel_grupo (id_papel, membro_do_grupo)
SELECT id_papel, currval('acl.sq_tb_papel')
FROM acl.tb_papel_grupo p
WHERE p.membro_do_grupo = (select id_papel from acl.tb_papel where ds_identificador = '/pages/Procuradoria/listView.seam');

END IF;

END $$;

/*
 * Thiago Vieira
 * Tipificação automática das defensorias
  */

UPDATE client.tb_procuradoria SET in_tipo_procuradoria = 'D' where ds_nome ilike '%defensoria%';

DO $$
BEGIN

IF NOT EXISTS 
	( SELECT 1 FROM   pg_class c JOIN   pg_namespace n ON n.oid = c.relnamespace WHERE  c.relname = 'tb_processo_parte_idx_proc' AND n.nspname = 'client') 

THEN
    
	CREATE INDEX tb_processo_parte_idx_proc ON client.tb_processo_parte (id_procuradoria);

END IF;

END $$;


DO $$
BEGIN

IF NOT EXISTS 

	(SELECT 1 FROM   pg_class c JOIN   pg_namespace n ON n.oid = c.relnamespace WHERE  c.relname = 'tb_proc_parte_exp_idx_procur' AND    n.nspname = 'client') 

THEN
    
	CREATE INDEX tb_proc_parte_exp_idx_procur ON client.tb_proc_parte_expediente (id_procuradoria);

END IF;

END $$;


/*
 * Alexander da Costa Monteiro
 */

DO $$

BEGIN
IF EXISTS
	(select 1 from acl.tb_papel
	 where ds_identificador = 'procurador')
THEN
	update acl.tb_papel
	set ds_nome = 'Representante processual'
	where ds_identificador = 'procurador';

   	update core.tb_localizacao  
	set ds_localizacao = replace(ds_localizacao, 'Procurador -', 'Representante processual -')
	where ds_localizacao like 'Procurador -%';

END IF;

END $$;
/*
 * Rodrigo Santos
 * 20/11/2014
 * Arquivo inicial dos scripts da versão 1.7.1.0
 */


CREATE OR REPLACE FUNCTION add_id_municipio_ibge() RETURNS void AS
$BODY$
BEGIN
IF NOT EXISTS
	(SELECT 1 FROM information_schema.columns	
		WHERE table_schema = 'core' 
		AND table_name = 'tb_municipio' 
		AND column_name = 'id_municipio_ibge')
THEN
	ALTER TABLE core.tb_municipio ADD COLUMN id_municipio_ibge varchar(10);
END IF;

END
$BODY$
LANGUAGE plpgsql VOLATILE STRICT;

SELECT add_id_municipio_ibge();

DROP FUNCTION add_id_municipio_ibge();

CREATE OR REPLACE FUNCTION add_nr_ano_eleicao() RETURNS void AS
$BODY$
BEGIN
IF EXISTS
        (SELECT 1 FROM information_schema.columns
                WHERE table_schema = 'client'
                AND table_name = 'tb_eleicao'
                AND column_name = 'nr_ano_aleicao')
THEN
        ALTER TABLE client.tb_eleicao DROP COLUMN nr_ano_aleicao;
END IF;

IF EXISTS
        (SELECT 1 FROM information_schema.columns
                WHERE table_schema = 'client'
                AND table_name = 'tb_eleicao'
                AND column_name = 'ds_ano_eleicao')
THEN
        ALTER TABLE client.tb_eleicao DROP COLUMN ds_ano_eleicao;
END IF;


IF NOT EXISTS
        (SELECT 1 FROM information_schema.columns
                WHERE table_schema = 'client'
                AND table_name = 'tb_eleicao'
                AND column_name = 'nr_ano_eleicao')
THEN
        ALTER TABLE client.tb_eleicao ADD COLUMN nr_ano_eleicao integer;
END IF;

END
$BODY$
LANGUAGE plpgsql VOLATILE STRICT;

SELECT add_nr_ano_eleicao();

DROP FUNCTION add_nr_ano_eleicao();
/**
 * Inclusão do campo id_instancia_origem em tb_processo_documento para guardar o id 
 * da instancia de origem quando o documento for criado em outra instância e remetido.
 */
CREATE OR REPLACE FUNCTION add_id_instancia_origem() RETURNS void AS
$BODY$
BEGIN
IF NOT EXISTS
	(SELECT 1 FROM information_schema.columns	
		WHERE table_schema = 'core' 
		AND table_name = 'tb_processo_documento' 
		AND column_name = 'id_instancia_origem')
THEN
	ALTER TABLE core.tb_processo_documento ADD COLUMN id_instancia_origem core.id;
END IF;

END
$BODY$
LANGUAGE plpgsql VOLATILE STRICT;

SELECT add_id_instancia_origem();

DROP FUNCTION add_id_instancia_origem();
/**
 * [PJEII-19739]
 */

DO $$

BEGIN
	ALTER TABLE client.tb_pessoa_juridica
	ALTER COLUMN nm_fantasia
	DROP NOT NULL;	
END $$;
/**
 * [PJEII-19607]
 * Altera o processo documento de uma instância diferente, passando o valor do campo número para id_instancia_origem
 * 
 */

CREATE OR REPLACE FUNCTION normalizaNumeroInstanciaOrigem()
RETURNS void AS $$
DECLARE 
   processoDocumentos RECORD;
BEGIN	
	-- atualiza os processsosDocumentos com o nr_documento
	   FOR processoDocumentos IN SELECT * FROM core.tb_processo_documento tpd
		WHERE exists (select 1 from core.tb_parametro p  where p.nm_variavel='aplicacaoSistema' and p.vl_variavel<>tpd.ds_instancia)
		and tpd.nr_documento is not null 
	   LOOP
		BEGIN
		      update core.tb_processo_documento 
		      set id_instancia_origem=nr_documento::INTEGER
		      WHERE
		      id_processo_documento = processoDocumentos.id_processo_documento;
		      RAISE NOTICE 'Documento atualizado. ProcessoDocumento "%"; Numero do documento "%"',processoDocumentos.id_processo_documento, processoDocumentos.nr_documento;
		EXCEPTION WHEN OTHERS THEN
			RAISE NOTICE 'Número documento inválido: "%".', processoDocumentos.nr_documento;
		END;
	   END LOOP;
    
RETURN ;
END;
$$ LANGUAGE plpgsql;


select normalizaNumeroInstanciaOrigem();


drop function normalizaNumeroInstanciaOrigem();
/*
 * PJEII-10364
 * Thiago Vieira
 * 20/11/2014
 * */

do $$
  begin
    begin
      alter table client.tb_proc_parte_expediente add column id_procuradoria integer;
    exception
      when duplicate_column then raise notice 'coluna id_procuradoria já existe na tabela tb_proc_parte_expediente.';
    end;
  end;
$$;

update client.tb_proc_parte_expediente pa
set id_procuradoria = ppe.id_procuradoria
from client.tb_pess_prcrdoria_entidade ppe
where pa.id_pessoa_parte = ppe.id_pessoa;

do $$
  begin
    begin
      alter table client.tb_processo_parte add column id_procuradoria integer;
    exception
      when duplicate_column then raise notice 'coluna id_procuradoria já existe na tabela tb_processo_parte.';
    end;
  end;
$$;

update client.tb_processo_parte pa
		set id_procuradoria = ppe.id_procuradoria
		from client.tb_pess_prcrdoria_entidade ppe
		where pa.id_pessoa = ppe.id_pessoa;

do $$
  begin
    begin
      alter table client.tb_procuradoria add column in_tipo_procuradoria character(1) NOT NULL DEFAULT 'P';
    exception
      when duplicate_column then raise notice 'coluna in_tipo_procuradoria já existe na tabela tb_procuradoria.';
    end;
  end;
$$;

DO $$

BEGIN
	IF NOT EXISTS (SELECT 1 FROM information_schema.sequences WHERE sequence_schema = 'client' AND sequence_name = 'sq_tb_pessoa_procuradoria') THEN
		CREATE SEQUENCE client.sq_tb_pessoa_procuradoria INCREMENT 1 MINVALUE 1 START 1 CACHE 1;
	END IF;

	CREATE TABLE if not exists client.tb_pessoa_procuradoria
(
  id_pessoa_procuradoria client.id NOT NULL DEFAULT nextval('client.sq_tb_pessoa_procuradoria'::regclass),
  id_pessoa client.id not null,
  id_procuradoria client.id not null,
  in_chefe_procuradoria boolean default false,
  CONSTRAINT tb_pessoa_procuradoria_pkey PRIMARY KEY (id_pessoa_procuradoria),
  CONSTRAINT tb_pessoa_procuradoria_id_pessoa_fkey FOREIGN KEY (id_pessoa)
      REFERENCES client.tb_pessoa_procurador (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT tb_pessoa_procuradoria_id_procuradoria_fkey FOREIGN KEY (id_procuradoria)
      REFERENCES client.tb_procuradoria (id_procuradoria) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

insert into client.tb_pessoa_procuradoria(id_pessoa,id_procuradoria,in_chefe_procuradoria)
select id,id_procuradoria,in_chefe_procuradoria from client.tb_pessoa_procurador where id_procuradoria is not null;


END $$;
/*
 * Alexander da Costa Monteiro
 * Data: 22/10/2014
 * PJEII-18008
 * Script de inserção do papel de Administrador de Procuradoria
 */

DO $$

BEGIN

	IF NOT EXISTS  (SELECT 1 FROM acl.tb_papel WHERE ds_identificador = 'pje:papel:administrarProcuradorias' )
	

	THEN

		insert into acl.tb_papel(id_papel, in_condicional, ds_identificador, ds_nome, in_ativo)
		values(nextval('acl.sq_tb_papel'), false, 'pje:papel:administrarProcuradorias', 'Administrador de Procuradoria', true);
		
	END IF;	


END $$;

/**
 * Rodrigo Santos Menezes
 * [PJEII-18813]
 */

DO $$

BEGIN
	IF NOT EXISTS (SELECT 1 FROM information_schema.sequences WHERE sequence_schema = 'client' AND sequence_name = 'sq_tb_pess_proc_jurisdicao') THEN
		CREATE SEQUENCE client.sq_tb_pess_proc_jurisdicao INCREMENT 1 MINVALUE 1 START 1 CACHE 1;
	END IF;

	CREATE TABLE if not exists client.tb_pess_proc_jurisdicao
(
  id_pess_proc_jurisdicao client.id NOT NULL DEFAULT nextval('client.sq_tb_pess_proc_jurisdicao'::regclass),
  id_jurisdicao client.id not null,
  id_pessoa_procuradoria client.id not null,
  in_ativo boolean default true,
  CONSTRAINT tb_pess_proc_jurisdicao_pkey PRIMARY KEY (id_pess_proc_jurisdicao),
  CONSTRAINT tb_pess_proc_jurisdicao_id_jurisdicao_fkey FOREIGN KEY (id_jurisdicao)
      REFERENCES client.tb_jurisdicao (id_jurisdicao) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT tb_pess_proc_jurisdicao_id_pessoa_procuradoria_fkey FOREIGN KEY (id_pessoa_procuradoria)
      REFERENCES client.tb_pessoa_procuradoria (id_pessoa_procuradoria) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

END $$;
/**
 * Rodrigo Santos Menezes
 * [PJEII-18825]
 */

DO $$

BEGIN
	IF NOT EXISTS (SELECT 1 FROM information_schema.sequences WHERE sequence_schema = 'client' AND sequence_name = 'sq_tb_caixa_representante') THEN
		CREATE SEQUENCE client.sq_tb_caixa_representante INCREMENT 1 MINVALUE 1 START 1 CACHE 1;
	END IF;

	CREATE TABLE if not exists client.tb_caixa_representante
(
  id_caixa_representante client.id NOT NULL DEFAULT nextval('client.sq_tb_caixa_representante'::regclass),
  id_pessoa_fisica client.id not null,
  id_caixa_adv_proc client.id not null,
  CONSTRAINT tb_caixa_representante_pkey PRIMARY KEY (id_caixa_representante),
  CONSTRAINT tb_pessoa_fisica_id_pessoa_fisica_fkey FOREIGN KEY (id_pessoa_fisica)
      REFERENCES client.tb_pessoa_fisica (id_pessoa_fisica) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT tb_caixa_adv_proc_id_caixa_adv_proc_fkey FOREIGN KEY (id_caixa_adv_proc)
      REFERENCES client.tb_caixa_adv_proc (id_caixa_adv_proc) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

END $$;
/**
 * Rodrigo Santos Menezes
 * [PJEII-18825]
 */

DO $$

BEGIN
	IF NOT EXISTS (SELECT 1 FROM information_schema.sequences WHERE sequence_schema = 'client' AND sequence_name = 'sq_tb_periodo_inativ_caixa_rep') THEN
		CREATE SEQUENCE client.sq_tb_periodo_inativ_caixa_rep INCREMENT 1 MINVALUE 1 START 1 CACHE 1;
	END IF;

	CREATE TABLE if not exists client.tb_periodo_inativ_caixa_rep
(
  id_periodo_inativ_caixa_rep client.id NOT NULL DEFAULT nextval('client.sq_tb_periodo_inativ_caixa_rep'::regclass),
  id_caixa_adv_proc client.id not null,
  dt_inicio client.data_hora,
  dt_fim client.data_hora,
  CONSTRAINT tb_periodo_inativ_caixa_rep_pkey PRIMARY KEY (id_periodo_inativ_caixa_rep),
  CONSTRAINT tb_caixa_adv_proc_id_caixa_adv_proc_fkey2 FOREIGN KEY (id_caixa_adv_proc)
      REFERENCES client.tb_caixa_adv_proc (id_caixa_adv_proc) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

END $$;
/*
 * PJEII-18820
 * Criação da tabela com o log do histórico da movimentação
 */
DO $$

BEGIN
	IF NOT EXISTS (SELECT 1 FROM information_schema.sequences WHERE sequence_schema = 'client' AND sequence_name = 'sq_tb_log_hist_movimentacao') THEN
		CREATE SEQUENCE client.sq_tb_log_hist_movimentacao INCREMENT 1 MINVALUE 1 START 1 CACHE 1;
	END IF;

	CREATE TABLE if not exists client.tb_log_hist_movimentacao
	(
	  id_log_hist_moviment client.id NOT NULL DEFAULT nextval('client.sq_tb_log_hist_movimentacao'::regclass),
	  dt_log timestamp without time zone NOT NULL,
	  id_processo_trf integer NOT NULL,
	  id_usuario integer,
	  id_caixa_adv_proc integer,
	  cd_motivo_movimentacao character(1) NOT NULL,
	  ds_texto_movimentacao varchar not null,
	  CONSTRAINT tb_log_historico_moviment_pkey PRIMARY KEY (id_log_hist_moviment),
	  CONSTRAINT tb_log_historico_moviment_id_processo_trf_fkey FOREIGN KEY (id_processo_trf)
	      REFERENCES client.tb_processo_trf (id_processo_trf) MATCH SIMPLE
	      ON UPDATE NO ACTION ON DELETE NO ACTION,
  	  CONSTRAINT tb_log_historico_moviment_id_usuario_fkey FOREIGN KEY (id_usuario)
	      REFERENCES core.tb_usuario (id_usuario) MATCH SIMPLE
	      ON UPDATE NO ACTION ON DELETE NO ACTION
	)
	WITH (
	  OIDS=FALSE
	);	

END $$;
/*
 * Rodrigo Santos Menezes
 * PJEII-18813
 */

do $$
  begin
    begin
      alter table client.tb_pessoa_procuradoria add column in_acompanha_sessao boolean default true;
    exception
      when duplicate_column then raise notice 'coluna in_acompanha_sessao já existe na tabela tb_pessoa_procuradoria.';
    end;
  end;
$$;
/**
 * Ronaldo Ferreira de Almeida
 * [PJEII-18836]
 */

DO $$

BEGIN
IF NOT EXISTS
	(SELECT 1 FROM information_schema.columns	
		WHERE table_schema = 'client' 
		AND table_name = 'tb_caixa_adv_proc' 
		AND column_name = 'vl_nr_processo_intervalo')
THEN

	ALTER TABLE client.tb_caixa_adv_proc ADD COLUMN vl_nr_processo_intervalo text;
	COMMENT ON COLUMN client.tb_caixa_adv_proc.vl_nr_processo_intervalo IS 'Valores possíveis de filtros a serem aplicados no número do processo';

END IF;

END $$;
