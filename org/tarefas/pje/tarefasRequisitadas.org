* Tassni
** Verificar se os documentos do processo 6008393-09.2014.8.13.0024, também estão elencados em outro processo.
  - id_processo: 15831
  - id_processo_documento_bin = 159164 e 159165

** Verificar que versões o PJe usa (se é que isso faz sentido em todos os casos) dos frameworks Jquery, JSF, Richfaces, JbossSeam, Hibernate e JBPM
   | JSF | Richfaces             | Hibernate                        | Seam        | JBPM |
   |-----+-----------------------+----------------------------------+-------------+------|
   |     | richfaces 3.3.3.Final | hibernate entitymanager 3.4.0.GA | 2.2.2.Final |      |
   |     |                       | hibernate core 3.3.1.GA          |             |      |
   |     |                       | hibernate search 3.1.1.GA        |             |      |
   |     |                       | hibernate validator 3.1.0.GA     |             |      |
		      


** Verificar como é a validação do jcr para unicidade de documentos, ou seja, se ele permite excluir o documento no banco e no filesystem e incluí-lo novamente.
   - processo: 6000001-46.2015.8.13.0024
   - doc: art70dia07060032014.pdf
   - hash: 3f6aaba84c0e24bc802223dbad71d36622d446f4
   - vara: 3ª Vara Regional do Barreiro
   - hash do novo upload do mesmo doc:
     3f6aaba84c0e24bc802223dbad71d36622d446f4

Email:

Boa tarde, Ildeci,

Fiz alguns testes no jcr para saber como é o seu comportamente em
relação a exclusão, ou perda de arquivos do sistema de arquivos, ou
por algum outro motivo o arquivo tenha sumido.

E descobri que se você copiar o arquivo desaparecido para  a pasta do
jcr e alterar o seu nome para o valor da coluna "nr_documento_storage"
da tabela "tb_processo_documento_bin" o download do mesmo volta a
funcionar. Assim podemos pedir o arquivo à usuária que o inlcuiu, copiá-lo para
o lugar em que ele deveria estar no jcr e renomeá-lo como explicado
acima. Acho que dessa maneira resolvemos o problema dela.

Eu não consegui achar o chamado correspondente a esse erro você lembra?


1)Inclui um arquivo em um processo
2)Depois exclui esse arquivo via sistema de arquivos diretamente da
pasta onde ele é armazenada no jcr
3)Exclui o arquivo através do painel documentos da aba processo no
pje(clicar na lixeira e dar um motivo para exclusão)
4)Inclui novamente o arquivo excluído através da aba "Incluir petições
e documentos" o arquivo foi incluído normalmente no sistema, ou seja
foram criadas as linhas no banco correpondestes a inclusão de um
arquivo. O mesmo hash foi atribuído ao arquivo, mas ele foi listado
como se não estivesse assinado.
5) Tentei fazer download do arquivo e deu erro como se ele não
   existisse.
6) Verifiquei e ele não tinho sido armazenado na pasta do jcr.
7) copiei o arquivo para a pasta do jcr e alterei seu nome para o
   hash(ou coluna nr_documento_storage da tabela tb_processo_documento_bin)
   atribuído a ele como deveria ser.
8) Atualizei a página do processo e consegui fazer o download do
   arquivo
9) Apaguei todas as linhas das tabelas tb_processo_documento e
   tb_processo_documento_bin referentes ao arquivo
10) Tentei fazer o upload dele novamente e não deu certo o arquivo
    novamente não foi copiado para a pasta do jcr

Após os testes pode se perceber que o jcr ou o pje de alguma forma impede que o
arquivo seja carregado mesmo que as linha do banco referentes ao
arquivo sejam apagadas.

* Renan
** Contar número de processos em tramitação

Órgãos julgadores ativos em Belo Horizonte = 72
Órgãos julgadores ativos em  Betim = 7
Órgãos julgadores ativos em Contagem = 7

Processos em tranmitação até dia 07/11/2014 sem contar com os processos baixados: 10527
Processos em tranmitação até dia 07/11/2014 contando com os processos baixados: 11149

#+begin_src sql
set SEARCH_PATH = acl, client, core, criminal, jt, public;

SELECT 
	count(*)
FROM
	tb_processo_trf proc_	
WHERE
	dt_distribuicao is not null
	-- INICIO: verifica se o processo foi baixado 
	AND NOT EXISTS (
		select 
			evt.ds_evento, evt.ds_movimento, proc_evt.dt_atualizacao
		from 			
			core.tb_processo_evento proc_evt,
			core.tb_evento evt
		where
			proc_.id_processo_trf = proc_evt.id_processo
			and proc_evt.id_evento = evt.id_evento
			and evt.ds_movimento ilike 'arquivado%'
			and dt_atualizacao = (
				select max(dt_atualizacao)
				from tb_processo_evento proc_evt2
				where  proc_evt2.id_processo = proc_.id_processo_trf
			)	
		order by 
			proc_evt.dt_atualizacao desc

	
	)
	
	;
	-- FIM: verifica se o processo foi baixado


SELECT 
	proc_.*, loc_.ds_localizacao
FROM
	tb_processo_trf proc_, 
	tb_localizacao loc_
WHERE
	dt_distribuicao is not null
	and loc_.id_localizacao = proc_.id_localizacao_inicial
	and proc_.dt_distribuicao < '2014/11/10'
	-- INICIO: verifica se o processo foi baixado 
	AND NOT EXISTS (
		select 
			evt.ds_evento, evt.ds_movimento, proc_evt.dt_atualizacao
		from 			
			core.tb_processo_evento proc_evt,
			core.tb_evento evt
		where
			proc_.id_processo_trf = proc_evt.id_processo
			and proc_evt.id_evento = evt.id_evento
			and evt.ds_movimento ilike 'arquivado%'
			and dt_atualizacao = (
				select max(dt_atualizacao)
				from tb_processo_evento proc_evt2
				where  proc_evt2.id_processo = proc_.id_processo_trf
			)	
		order by 
			proc_evt.dt_atualizacao desc

	
	)
	
	;
	-- FIM: verifica se o processo foi baixado
	
	

-- nao verifica se processos foram baixados
SELECT 
	count(*)
FROM
	tb_processo_trf proc_
WHERE
	dt_distribuicao is not null
	and proc_.dt_distribuicao < '2014/11/10';


#+end_src


* Oldair
** DONE verificar tipos reditribuicao
** TODO verificar cadastro duplicado de advogado
   - Tabelas: tb_pessoa_assistente_adv, tb_pess_assistente_procurd,
     tb_pessoa_magistrado, tb_pessoa_oficial_justica,
     tb_pess_assistente_procurd, tb_pessoa_procurador,
     tb_pessoa_servidor, tb_pessoa_perito
* Talles
** TODO Verificar como o pje faz o download de documentos
* Pamela
** TODO verficar pq o pje nao esta funcionando no ambiente dela
