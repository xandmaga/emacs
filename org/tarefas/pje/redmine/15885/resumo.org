Roteiro de Testes MNI

Ambiente de Teste: https://pje-jcr.intra.tjmg.gov.br/pje/intercomunicacao?wsdl

Versão MNI: 2.2.2

Versão PJE: 1.7.1.0

Perfis:

    Advogado: 38443057343, 02500079759
    Juiz: 77201680625, 38030861672
    Promotor: 29657652634
    Servidor(Escrivão): 08870255603, 06285342644

Testes:

    Protocolar novo processo
        Perfil advogado
            Protocolando a partir de 6016473-25.2015.8.13.0024
            Protocolado com sucesso: <NUMERO_PROTOCOLO_E_PROCESSO_CRIADO>
        Perfil promotor
            Protocolando a partir de <NUMERO_PROCESSO>
            Protocolado com sucesso: <NUMERO_PROTOCOLO_E_PROCESSO_CRIADO>
    Consultar processo ordinário por perfil
        Perfil advogado
            Consultar processo <NUMERO_PROCESSO>
            <MENSAGEM DA CONSULTA>
        Perfil juiz
            Consultar processo <NUMERO_PROCESSO>
            <MENSAGEM DA CONSULTA>
        Perfil promotor
            Consultar processo <NUMERO_PROCESSO>
            <MENSAGEM DA CONSULTA>
        Perfil servidor
            Consultar processo <NUMERO_PROCESSO>
            <MENSAGEM DA CONSULTA>
    Consultar processo com segredo de justiça por perfil
        Perfil advogado
            Consultar processo <NUMERO_PROCESSO_SEGREDO>
            <MENSAGEM DA CONSULTA>
        Perfil Juiz
            Consultar processo <NUMERO_PROCESSO_SEGREDO>
            <MENSAGEM DA CONSULTA>
        Perfil Promotor
            Consultar processo <NUMERO_PROCESSO_SEGREDO>
            <MENSAGEM DA CONSULTA>
        Perfil Servidor
            Consultar processo <NUMERO_PROCESSO_SEGREDO>
            <MENSAGEM DA CONSULTA>
    Entregar manifestação processual em processo provocado por intimação
        Processo com intimação para o Ministério Público <NUMERO_PROCESSO>
        <MENSAGEM_CONSULTA_AVISOS_PENDENTES>
        <MENSAGEM_CONSULTA_TEOR_COMUNICACAO>
        <MENSAGEM_ENTREGA_MANISFETACAO_PROCESSUAL>
    Entregar manifestação processual em processo com segredo de justiça provocado por intimação
        Processo com intimação para o Ministério Público <NUMERO_PROCESSO>
        <MENSAGEM_CONSULTA_TEOR_COMUNICACAO> 
        <MENSAGEM_ENTREGA_MANISFETACAO_PROCESSUAL>
        <MENSAGEM_CONSULTA_AVISOS_PENDENTES>
    Entregar manifestação processual em processo ordinário
        Perfil advogado
            <NUMERO_PROCESSO>
            <MENSAGEM_ENTREGA_MANIFESTACAO>
        Perfil juiz
            <NUMERO_PROCESSO>
            <MENSAGEM_ENTREGA_MANIFESTACAO>
        Perfil promotor
            <NUMERO_PROCESSO>
            <MENSAGEM_ENTREGA_MANIFESTACAO>
        Perfil servidor
            <NUMERO_PROCESSO>
            <MENSAGEM_ENTREGA_MANIFESTACAO>
    Entregar manifestação processual em processo em segredo de justiça
        Perfil advogado
            <NUMERO_PROCESSO_SEGREDO>
            <MENSAGEM_ENTREGA_MANIFESTACAO>
        Perfil juiz
            <NUMERO_PROCESSO_SEGREDO>
            <MENSAGEM_ENTREGA_MANIFESTACAO>
        Perfil promotor
            <NUMERO_PROCESSO_SEGREDO>
            <MENSAGEM_ENTREGA_MANIFESTACAO>
        Perfil servidor
            <NUMERO_PROCESSO_SEGREDO>
            <MENSAGEM_ENTREGA_MANIFESTACAO>
