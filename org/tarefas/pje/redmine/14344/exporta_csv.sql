﻿/*exporta tabelas para um csv*/
CREATE OR REPLACE FUNCTION db_to_csv(path TEXT) RETURNS void AS $$
declare
  tables RECORD;
  statement TEXT;
begin
  FOR tables IN 
    SELECT (table_schema || '.' || table_name) AS schema_table
    FROM information_schema.tables t INNER JOIN information_schema.schemata s 
    ON s.schema_name = t.table_schema 
    WHERE t.table_name IN ('tb_classe_judicial', 'tb_assunto_trf', 'tb_competencia', 'tb_orgao_julgador', 'tb_dimensao_alcada', 'tb_aplicacao_classe', 'tb_jurisdicao', 'tb_localizacao', 'tb_endereco', 'tb_estado', 'tb_cep', 'tb_fluxo', 'tb_tipo_audiencia', 'tb_tipo_parte', 'tb_tipo_parte_trf', 'tb_usuario', 'tb_municipio', 'tb_usuario_login')
    ORDER BY schema_table
  LOOP
    statement := 'COPY ' || tables.schema_table || ' TO ''' || path || '\' || tables.schema_table || '.csv' || ''' WITH (FORMAT CSV, HEADER TRUE)';
    EXECUTE statement;
  END LOOP;
  return;  
end;
$$ LANGUAGE plpgsql;
SELECT db_to_csv('C:\Users\t0085324\workspace\bitbucket\emacs\org\tarefas\pje\redmine\14344\pjesup');

/*exporta colunas que sao id das tabelas para um csv*/
CREATE OR REPLACE FUNCTION db_to_csv(path TEXT) RETURNS void AS $$
declare
  tables RECORD;
  statement TEXT;
begin
  FOR tables IN 
    SELECT (table_schema || '.' || table_name) AS schema_table
    FROM information_schema.tables t INNER JOIN information_schema.schemata s 
    ON s.schema_name = t.table_schema 
    WHERE t.table_name IN ('tb_classe_judicial', 'tb_assunto_trf', 'tb_competencia', 'tb_orgao_julgador', 'tb_dimensao_alcada', 'tb_aplicacao_classe', 'tb_jurisdicao', 'tb_localizacao', 'tb_endereco', 'tb_estado', 'tb_cep', 'tb_fluxo', 'tb_tipo_audiencia', 'tb_tipo_parte', 'tb_tipo_parte_trf', 'tb_usuario', 'tb_municipio', 'tb_usuario_login')
    ORDER BY schema_table
  LOOP
    statement := 'COPY ' || tables.schema_table || '(' || (SELECT string_agg(a.attname, ',') FROM   pg_index i JOIN   pg_attribute a ON a.attrelid = i.indrelid AND a.attnum = ANY(i.indkey) WHERE  i.indrelid = tables.schema_table::regclass AND i.indisprimary) || ')' || ' TO ''' || path || '\' || tables.schema_table || '_ids' || '.csv' ||''' DELIMITER '';'' CSV HEADER';
    EXECUTE statement;
  END LOOP;
  return;  
end;
$$ LANGUAGE plpgsql;
SELECT db_to_csv('C:\Users\t0085324\workspace\bitbucket\emacs\org\tarefas\pje\redmine\14344\pjesup');