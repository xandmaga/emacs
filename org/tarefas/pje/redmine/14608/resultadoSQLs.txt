/*
 * tabela tb_orgao_julgador_cargo
 ******************************************/


    select
        id_orgao_julgador_cargo
    from
        tb_orgao_julgador_cargo
    where
        id_orgao_julgador_cargo =? for update
		
    
	update
        tb_orgao_julgador_cargo
    set
        nr_acumulador_distribuicao=?,
        nr_acumulador_processo=?,
        in_ativo=?,
        in_auxiliar=?,
        id_cargo=?,
        in_colegiado=?,
        ds_cargo=?,
        id_orgao_julgador=?,
        nr_peso_distribuicao=?,
        in_recebe_distribuicao=?,
        cd_sigla_cargo=?,
        vl_peso=?
    where
        id_orgao_julgador_cargo=?

/*********************************************/
		
    select
        nextval ('sq_tb_log')

		
    select
        nextval ('sq_tb_log_detalhe')

		
    select
        nextval ('sq_tb_log_detalhe')		
		
    insert
    into
        tb_log
        (dt_log, ds_id_entidade, id_usuario, ds_ip, ds_entidade, ds_package, tp_operacao, id_pagina, id_log)
    values
        (?, ?, ?, ?, ?, ?, ?, ?, ?)

		
    insert
    into
        tb_log_detalhe
        (id_log, nm_atributo, ds_valor_anterior, ds_valor_atual, id_log_detalhe)
    values
        (?, ?, ?, ?, ?)

		
    insert
    into
        tb_log_detalhe
        (id_log, nm_atributo, ds_valor_anterior, ds_valor_atual, id_log_detalhe)
    values
        (?, ?, ?, ?, ?)	


   insert
    into
        tb_processo_trf_log
        (dt_log, id_processo_trf, id_processo_trf_log)
    values
        (?, ?, ?)

		
		
    insert
    into
        tb_processo_trf_log_dist
        (in_tipo_distribuicao, id_orgao_julgador, id_orgao_julgador_cargo, id_orgao_julgador_colegiado, id_processo_trf_log)
    values
        (?, ?, ?, ?, ?)

		
		
		
    insert
    into
        tb_items_log
        (dt_item, in_criticidade, ds_item, id_processo_trf_log, id_item_log)
    values
        (?, ?, ?, ?, ?)

		
					
    insert
    into
        tb_items_log
        (dt_item, in_criticidade, ds_item, id_processo_trf_log, id_item_log)
    values
        (?, ?, ?, ?, ?)

		
		
    insert
    into
        tb_items_log
        (dt_item, in_criticidade, ds_item, id_processo_trf_log, id_item_log)
    values
        (?, ?, ?, ?, ?)

		
		

    insert
    into
        tb_items_log
        (dt_item, in_criticidade, ds_item, id_processo_trf_log, id_item_log)
    values
        (?, ?, ?, ?, ?)

		
		
    insert
    into
        tb_items_log
        (dt_item, in_criticidade, ds_item, id_processo_trf_log, id_item_log)
    values
        (?, ?, ?, ?, ?)

		
		
    insert
    into
        tb_items_log
        (dt_item, in_criticidade, ds_item, id_processo_trf_log, id_item_log)
    values
        (?, ?, ?, ?, ?)

		
		
		
    insert
    into
        tb_items_log
        (dt_item, in_criticidade, ds_item, id_processo_trf_log, id_item_log)
    values
        (?, ?, ?, ?, ?)

		
		
    insert
    into
        tb_items_log
        (dt_item, in_criticidade, ds_item, id_processo_trf_log, id_item_log)
    values
        (?, ?, ?, ?, ?)

		
		
		
    insert
    into
        tb_items_log
        (dt_item, in_criticidade, ds_item, id_processo_trf_log, id_item_log)
    values
        (?, ?, ?, ?, ?)

		
		
    insert
    into
        tb_items_log
        (dt_item, in_criticidade, ds_item, id_processo_trf_log, id_item_log)
    values
        (?, ?, ?, ?, ?)

		
		
/********************************************************************************************************************
				
		
    update
        tb_processo_trf
    set
        nr_ano=?,
        in_apreciado_justica_gratuita=?,
        in_apreciado_segredo=?,
        in_apreciado_sigilo=?,
        in_apreciado_tutela_liminar=?,
        id_cargo=?,
        id_classe_judicial=?,
        id_cl_judicial_outra_instancia=?,
        id_competencia=?,
        dt_autuacao=?,
        dt_distribuicao=?,
        dt_sugestao_sessao=?,
        ds_proc_referencia=?,
        in_deve_marcar_audiencia=?,
        dt_transitado_julgado=?,
        id_endereco_wsdl=?,
        id_estrutura_inicial=?,
        in_outra_instancia=?,
        in_inicial=?,
        nr_instancia=?,
        in_incidente=?,
        id_jurisdicao=?,
        in_justica_gratuita=?,
        id_localizacao_inicial=?,
        in_mandado_devolvido=?,
        id_municipio_fato_principal=?,
        nr_digito_verificador=?,
        nr_identificacao_orgao_justica=?,
        nr_origem_processo=?,
        nr_sequencia=?,
        ds_objeto=?,
        ds_observacao_segredo=?,
        id_orgao_julgador=?,
        id_orgao_julgador_cargo=?,
        id_orgao_julgador_colegiado=?,
        id_orgao_julgador_revisor=?,
        id_pes_apreciou_jus_gratuita=?,
        id_pessoa_marcou_julgamento=?,
        id_pessoa_marcou_pauta=?,
        id_pessoa_marcou_revisado=?,
        id_pessoa_relator_processo=?,
        id_proc_referencia=?,
        cd_processo_status=?,
        in_pronto_revisao=?,
        in_revisado=?,
        in_segredo_justica=?,
        in_selecionado_julgamento=?,
        in_selecionado_pauta=?,
        id_sessao_sugerida=?,
        in_tutela_liminar=?,
        vl_causa=?,
        vl_peso_distribuicao=?,
        vl_peso_processual=?,
        in_violacao_faixa_valores=?
    where
        id_processo_trf=?

		

/**********************************************************************************************		
		
    select
        nextval ('sq_tb_log')

		
		
    select
        nextval ('sq_tb_log_detalhe')

		
		
    insert
    into
        tb_log
        (dt_log, ds_id_entidade, id_usuario, ds_ip, ds_entidade, ds_package, tp_operacao, id_pagina, id_log)
    values
        (?, ?, ?, ?, ?, ?, ?, ?, ?)

		
		
    insert
    into
        tb_log_detalhe
        (id_log, nm_atributo, ds_valor_anterior, ds_valor_atual, id_log_detalhe)
    values
        (?, ?, ?, ?, ?)

		
/*********************************************************************************************************		
		
    update
        tb_processo_instance
    set
        in_ativo=?,
        id_processo=?,
        id_orgao_julgador=?,
        id_orgao_julgador_cargo=?,
        id_orgao_julgador_colegiado=?
    where
        id_proc_inst=?


/*********************************************************************************************************
		
		
    select
        nextval ('sq_tb_log')

		
		
    select
        nextval ('sq_tb_log_detalhe')

		
		
    select
        nextval ('sq_tb_log_detalhe')

		
		
    insert
    into
        tb_log
        (dt_log, ds_id_entidade, id_usuario, ds_ip, ds_entidade, ds_package, tp_operacao, id_pagina, id_log)
    values
        (?, ?, ?, ?, ?, ?, ?, ?, ?)

		
		
    insert
    into
        tb_log_detalhe
        (id_log, nm_atributo, ds_valor_anterior, ds_valor_atual, id_log_detalhe)
    values
        (?, ?, ?, ?, ?)

		
		
    insert
    into
        tb_log_detalhe
        (id_log, nm_atributo, ds_valor_anterior, ds_valor_atual, id_log_detalhe)
    values
        (?, ?, ?, ?, ?)

		
/*********************************************************************************************************
		
    update
        tb_orgao_julgador_cargo
    set
        nr_acumulador_distribuicao=?,
        nr_acumulador_processo=?,
        in_ativo=?,
        in_auxiliar=?,
        id_cargo=?,
        in_colegiado=?,
        ds_cargo=?,
        id_orgao_julgador=?,
        nr_peso_distribuicao=?,
        in_recebe_distribuicao=?,
        cd_sigla_cargo=?,
        vl_peso=?
    where
        id_orgao_julgador_cargo=?

/*********************************************************************************************************		
		
		
    select
        nextval ('sq_tb_log')

		
		
    select
        nextval ('sq_tb_log_detalhe')

		
		
    select
        nextval ('sq_tb_log_detalhe')

		
    insert
    into
        tb_log
        (dt_log, ds_id_entidade, id_usuario, ds_ip, ds_entidade, ds_package, tp_operacao, id_pagina, id_log)
    values
        (?, ?, ?, ?, ?, ?, ?, ?, ?)
		

		
    insert
    into
        tb_log_detalhe
        (id_log, nm_atributo, ds_valor_anterior, ds_valor_atual, id_log_detalhe)
    values
        (?, ?, ?, ?, ?)

		
    insert
    into
        tb_log_detalhe
        (id_log, nm_atributo, ds_valor_anterior, ds_valor_atual, id_log_detalhe)
    values
        (?, ?, ?, ?, ?)		