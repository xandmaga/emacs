﻿set search_path = public, acl, core, client, criminal, jt;

/*
 * Flavio Alves Reis 
 * 14/07/2014
 * [PJEII-16456] - Correções pendentes da issue PJEII-1641
 * */

-- Criar o papel Reclassificar tipos de documentos
INSERT INTO acl.tb_papel(in_condicional, ds_identificador, ds_nome, in_ativo)
SELECT FALSE, '/pages/Processo/Fluxo/documento/reclassificar.seam', 'Reclassificar tipos de documentos', TRUE
WHERE NOT EXISTS(SELECT id_papel FROM acl.tb_papel WHERE ds_identificador = '/pages/Processo/Fluxo/documento/reclassificar.seam');

-- Vincular o papel Administrador ao papel Reclassificar tipos de documentos
INSERT INTO acl.tb_papel_grupo (id_papel, membro_do_grupo)
SELECT 	(select p.id_papel from acl.tb_papel p where p.ds_identificador = 'admin'), 
	(select m.id_papel from acl.tb_papel m where m.ds_identificador = '/pages/Processo/Fluxo/documento/reclassificar.seam')
WHERE NOT EXISTS
(
	SELECT * FROM acl.tb_papel_grupo
	WHERE id_papel = (select p.id_papel from acl.tb_papel p where p.ds_identificador = 'admin')
	AND membro_do_grupo = (select m.id_papel from acl.tb_papel m where m.ds_identificador = '/pages/Processo/Fluxo/documento/reclassificar.seam')
);

-- Vincular o papel Pode reclassificar documentos ao papel Reclassificar tipos de documentos
INSERT INTO acl.tb_papel_grupo (id_papel, membro_do_grupo)
SELECT 	(select p.id_papel from acl.tb_papel p where p.ds_identificador = 'pje:papel:podeReclassificarDocumento'), 
	(select m.id_papel from acl.tb_papel m where m.ds_identificador = '/pages/Processo/Fluxo/documento/reclassificar.seam')
WHERE NOT EXISTS
(
	SELECT * FROM acl.tb_papel_grupo
	WHERE id_papel = (select p.id_papel from acl.tb_papel p where p.ds_identificador = 'pje:papel:podeReclassificarDocumento')
	AND membro_do_grupo = (select m.id_papel from acl.tb_papel m where m.ds_identificador = '/pages/Processo/Fluxo/documento/reclassificar.seam')
);
/*
 * Leonardo Inacio
 * 23/07/2014
 * [TJDFT] PJEII-17798 - Incluir no comprovante de protocolo (disponibilizado apos a distribuicao), no item Audiencia, texto complementar
 */
 

INSERT INTO core.tb_parametro (nm_variavel,ds_variavel,vl_variavel,in_sistema,in_ativo)
SELECT 'pje:processo:comprovanteProtocolo:audiencia:textoComplementar','Texto complementar que é exibido no item Audiência no Comprovante de Protocolo','',false,true
WHERE NOT EXISTS (SELECT 1 FROM core.tb_parametro WHERE nm_variavel = 'pje:processo:comprovanteProtocolo:audiencia:textoComplementar');
/*
 * Antonio Francisco Osorio
 * 28/07/2014
 * [PJEII-17903] 
 * Criar o parametro 'pje:agrupador:docsNaoLidos:papeis'
 */

INSERT INTO core.tb_parametro (nm_variavel,ds_variavel,vl_variavel,in_sistema,in_ativo)
SELECT 'pje:agrupador:docsNaoLidos:papeis','Identificação dos papéis, separados por virgula, que terão seus documentos juntos exibidos no agrupador de documentos não lidos','advogado,Perito,procurador',true,true 
WHERE NOT EXISTS (SELECT 1 FROM core.tb_parametro WHERE nm_variavel = 'pje:agrupador:docsNaoLidos:papeis');
/*
 * Leonardo Inacio Costa
 * 01/08/2014
 * [TJDFT] PJEII-17871 - Geracao automatica de movimentacao no momento da distribuicao do mandado ao oficial 
 * pelo oficial-distribuidor e na devolucao do mandado a secretaria pelo oficial
 */

INSERT INTO core.tb_parametro (nm_variavel,ds_variavel,vl_variavel,in_sistema,in_ativo)
SELECT 'pje:centralmandado:registrarMovimentacaoDistribuicaoMandado','Informa se o sistema deve registrar movimentacao na distribuicao de mandado ao oficial pelo oficial-distribuidor','false',false,true
WHERE NOT EXISTS (SELECT 1 FROM core.tb_parametro WHERE nm_variavel = 'pje:centralmandado:registrarMovimentacaoDistribuicaoMandado');

INSERT INTO core.tb_parametro (nm_variavel,ds_variavel,vl_variavel,in_sistema,in_ativo)
SELECT 'pje:centralmandado:registrarMovimentacaoDevolucaoMandado','Informa se o sistema deve registrar movimentacao na devolucao de mandado a secretaria pelo oficial','false',false,true
WHERE NOT EXISTS (SELECT 1 FROM core.tb_parametro WHERE nm_variavel = 'pje:centralmandado:registrarMovimentacaoDevolucaoMandado');

INSERT INTO core.tb_parametro (nm_variavel,ds_variavel,vl_variavel,in_sistema,in_ativo)
SELECT 'pje:movimento:codigo:recebidoMandadoCumprimento','Codigo da movimentacao de recebimento de mandado para cumprimento','985',false,true
WHERE NOT EXISTS (SELECT 1 FROM core.tb_parametro WHERE nm_variavel = 'pje:movimento:codigo:recebidoMandadoCumprimento');

INSERT INTO core.tb_parametro (nm_variavel,ds_variavel,vl_variavel,in_sistema,in_ativo)
SELECT 'pje:movimento:codigo:mandadoDevolvido','Codigo da movimentacao de mandado devolvido','106',false,true
WHERE NOT EXISTS (SELECT 1 FROM core.tb_parametro WHERE nm_variavel = 'pje:movimento:codigo:mandadoDevolvido');
/*
 * Augusto Martins
 * 08/09/2014
 * PJEII-17882 - Incluir parâmetro validaProcessoReferencia para permitir validações no número do processo de referência
 */
 

INSERT INTO core.tb_parametro (nm_variavel,ds_variavel,vl_variavel,in_sistema,in_ativo)
SELECT 'validaProcessoReferencia','Valida o nr do processo de referência (NA: não se aplica; DV: valida o dv; OR: valida o processo na origem)','NA',false,true
WHERE NOT EXISTS (SELECT 1 FROM core.tb_parametro WHERE nm_variavel = 'validaProcessoReferencia');
 /*
* Thiago Vieira
* 29/08/2014
* Inclusão da coluna in_ativo ta tabela tb_processo_evento
* */

do $$
  begin
    begin
      alter table core.tb_processo_evento add column in_ativo boolean default true;
    exception
      when duplicate_column then raise notice 'coluna in_ativo já existe na tabela tb_processo_evento.';
    end;
  end;
$$;
/*
 * Thiago Vieira
 * 09/09/2014
 * PJEII-17880 - 
 */
 

INSERT INTO core.tb_parametro (nm_variavel,ds_variavel,vl_variavel,in_sistema,in_ativo)
SELECT 'pje:painel:ultimasTarefas:quantidade','Retorna a quantidade máxima de registros na tabela do agrupador Últimas tarefas realizadas do painel do usuário','10',false,true
WHERE NOT EXISTS (SELECT 1 FROM core.tb_parametro WHERE nm_variavel = 'pje:painel:ultimasTarefas:quantidade');
/*
 * Alisson Melo Nascimento
 * 12/09/2014
 * PJEII-11548 - Adicionado acesso por login e senha ao PJE.
 * Colunas que farao o controle do cadastro e da ativacao de senhas
 */

do $$
  begin
    begin
      alter table acl.tb_usuario_login add column hash_ativacao_senha character varying(255);
    exception
      when duplicate_column then raise notice 'coluna hash_ativacao_senha já existe na tabela acl.tb_usuario_login.';
    end;
  end;
$$;

do $$
  begin
    begin
      alter table acl.tb_usuario_login add column in_status_senha character(1) NOT NULL DEFAULT 'I'::bpchar;
    exception
      when duplicate_column then raise notice 'coluna in_status_senha já existe na tabela acl.tb_usuario_login.';
    end;
  end;
$$;

do $$
  begin
    begin
      alter table acl.tb_usuario_login add column dt_validade_senha date;
    exception
      when duplicate_column then raise notice 'coluna dt_validade_senha já existe na tabela acl.tb_usuario_login.';
    end;
  end;
$$;
/*
 * Alisson Melo Nascimento
 * 12/09/2014
 * PJEII-11548 - Adicionado acesso por login e senha ao PJE.
 * Tabela q armazenara as tentativas de logon do usuario
 */

CREATE SEQUENCE acl.sq_tb_log_acesso
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE acl.tb_log_acesso
(
  id_log_acesso integer NOT NULL DEFAULT nextval('acl.sq_tb_log_acesso'::regclass),
  dt_evento timestamp without time zone NOT NULL,
  in_bem_sucedido boolean NOT NULL,
  id_usuario integer NOT NULL,
  CONSTRAINT tb_registro_logon_pkey PRIMARY KEY (id_log_acesso),
  CONSTRAINT tb_registr_logon_id_usuario FOREIGN KEY (id_usuario)
      REFERENCES acl.tb_usuario_login (id_usuario) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);
/*
 * Alisson Melo Nascimento
 * 12/09/2014
 * PJEII-11548 - adicionado acesso por login e senha ao PJE.
 * Esse script inativa todas as senha p/ obrigar o usuario a cadastrar uma nova.
 */
update acl.tb_usuario_login
set in_status_senha = 'I', dt_validade_senha = '2015-01-02'
where ds_senha is not null;

INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.0.10','CREATE TABLE TB LOG ACESSO','SQL','1.7.0.0/PJE_1.7.0.0_10__CREATE_TABLE_TB_LOG_ACESSO.sql',-334585943,'postgres','2014-09-12 20:54:04.018',83,258,272,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.0.11','INATIVAR SENHAS','SQL','1.7.0.0/PJE_1.7.0.0_11__INATIVAR_SENHAS.sql',-568827502,'postgres','2014-09-12 20:54:04.119',6,259,273,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.0.2','INC RECLASS TIP DOC','SQL','1.7.0.0/PJE_1.7.0.0_2__INC_RECLASS_TIP_DOC.sql',-893917142,'postgres','2014-09-12 20:54:02.769',232,250,264,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.0.3','ADICIONAR PARAMETRO TEXTO COMPLEMENTAR AUDIENCIA PROTOCOLO','SQL','1.7.0.0/PJE_1.7.0.0_3__ADICIONAR_PARAMETRO_TEXTO_COMPLEMENTAR_AUDIENCIA_PROTOCOLO.sql',1116217717,'postgres','2014-09-12 20:54:03.041',176,251,265,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.0.4','PARAMETRO PJE AGRUPADOR DOCS NAO LIDOS PAPEIS','SQL','1.7.0.0/PJE_1.7.0.0_4__PARAMETRO_PJE_AGRUPADOR_DOCS_NAO_LIDOS_PAPEIS.sql',1962711007,'postgres','2014-09-12 20:54:03.252',4,252,266,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.0.5','ADICIONAR PARAMETROS CENTRAL MANDADOS','SQL','1.7.0.0/PJE_1.7.0.0_5__ADICIONAR_PARAMETROS_CENTRAL_MANDADOS.sql',1029546287,'postgres','2014-09-12 20:54:03.286',8,253,267,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.0.6','ADICIONAR PARAMETRO VALIDA PROCESSO REFERENCIA','SQL','1.7.0.0/PJE_1.7.0.0_6__ADICIONAR_PARAMETRO_VALIDA_PROCESSO_REFERENCIA.sql',1167335888,'postgres','2014-09-12 20:54:03.321',3,254,268,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.0.7','TB PROCESSO EVENTO ADD COLUMN ATIVO','SQL','1.7.0.0/PJE_1.7.0.0_7__TB_PROCESSO_EVENTO_ADD_COLUMN_ATIVO.sql',432995850,'postgres','2014-09-12 20:54:03.35',497,255,269,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.0.8','ADICIONAR PARAMETRO QUANTIDADE PROCESSOS PESQUISA','SQL','1.7.0.0/PJE_1.7.0.0_8__ADICIONAR_PARAMETRO_QUANTIDADE_PROCESSOS_PESQUISA.sql',832567423,'postgres','2014-09-12 20:54:03.87',4,256,270,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.0.9','ADD COLUNAS ATIVACAO SENHA','SQL','1.7.0.0/PJE_1.7.0.0_9__ADD_COLUNAS_ATIVACAO_SENHA.sql',124986960,'postgres','2014-09-12 20:54:03.892',107,257,271,true);
