﻿set search_path = public, acl, core, client, criminal, jt;

/*
 * Luciano Tenylson Nogueira Costa
 * Data: 18/07/2014
 * PJEII-17397
 */
CREATE OR REPLACE FUNCTION add_in_intima_pessoal() RETURNS void AS
$BODY$
BEGIN

IF NOT EXISTS
	(SELECT 1 FROM information_schema.columns	
		WHERE table_schema = 'client' 
		AND table_name = 'tb_proc_parte_expediente' 
		AND column_name = 'in_intima_pessoal')
THEN

	ALTER TABLE client.tb_proc_parte_expediente ADD COLUMN in_intima_pessoal boolean NOT NULL DEFAULT false;
	UPDATE client.tb_proc_parte_expediente SET in_intima_pessoal = false WHERE in_intima_pessoal is null;

	

END IF;

END
$BODY$
LANGUAGE plpgsql VOLATILE STRICT;

SELECT add_in_intima_pessoal();

DROP FUNCTION add_in_intima_pessoal();

/*
 * Weberson Gabriel
 * 10/09/2014
 * PJEII-18145 - Incluir parâmetro "pje:papel:isMagistradoPadrao" para permitir indicar que o papel de magistrado sera o padrao na combo de papeis, caso o usuario logado tenha este papel 
 */
 

INSERT INTO core.tb_parametro (nm_variavel,ds_variavel,vl_variavel,in_sistema,in_ativo)
SELECT 'pje:papel:isMagistradoPadrao','Indica se o papel padrao na combo de papeis sera o Magistrado, caso o usuario logado tenha este papel (true ou false)','false',false,true
WHERE NOT EXISTS (SELECT 1 FROM core.tb_parametro WHERE nm_variavel = 'pje:papel:isMagistradoPadrao');
 
/*
* Luciano Côrtes
* 12/09/2014
* Inclusão da coluna in_ativo ta tabela tb_pess_gpo_oficial_jstica
* */

do $$
  begin
    begin
      alter table client.tb_pess_gpo_oficial_jstica add column in_ativo boolean default true;
    exception
      when duplicate_column then raise notice 'coluna in_ativo já existe na tabela tb_pess_gpo_oficial_jstica.';
    end;
  end;
$$;

/*
 * PJEII-18568
 * Rodrigo Santos Menezes
 * 10/11/2014
 * */

CREATE OR REPLACE FUNCTION executar_migracao() RETURNS void AS
$BODY$
BEGIN
IF NOT EXISTS (SELECT 1 FROM acl.tb_papel WHERE ds_identificador = 'pje:processo:visualizaPeticionamentoAvulso')
THEN
	INSERT 
		INTO acl.tb_papel 
			(id_papel, in_condicional, ds_identificador, ds_nome, in_ativo)
		VALUES
			(nextval('acl.sq_tb_papel'), false, 'pje:processo:visualizaPeticionamentoAvulso', 'Papel referente a visualizacao da aba Peticionamento Avulso',true);
END IF;
END
$BODY$
LANGUAGE plpgsql VOLATILE STRICT;

SELECT executar_migracao();
DROP FUNCTION executar_migracao();

/*
 * [PJEII-18228][TJDFT] Exibir alerta de proximidade para expiração do certificado digital 
 * Orlando Resende
 * 13/11/2014
 * Descrição: Indica a quantidade em dias (valor inteiro) para exibir alerta de expiração do certificado do usuário. Para nunca exibir o alerta, basta configurar com o valor -1.
 *
 */
 

INSERT INTO core.tb_parametro (nm_variavel,ds_variavel,vl_variavel,in_sistema,in_ativo)
SELECT 'qtdDiasAlertaExpiracaoCertificado','Indica a quantidade em dias (valor inteiro) para exibir alerta de expiração do certificado do usuário. Para nunca exibir o alerta, basta configurar com o valor -1.','-1',true,true
WHERE NOT EXISTS (SELECT 1 FROM core.tb_parametro WHERE nm_variavel = 'qtdDiasAlertaExpiracaoCertificado');

/*
 * Adriano Pamplona
 * Data: 04/11/2014
 * Issue: [PJEII-17577]
 * 
 * Cria os parâmetros para os tipos de documento arrolados abaixo:
 * - notificação; 
 * - vista para manifestação; 
 * - urgente; e 
 * - pauta de audiência/julgamento.
 */

INSERT INTO core.tb_parametro (nm_variavel,ds_variavel,vl_variavel,in_sistema,in_ativo)
	SELECT 
		'idTipoProcessoDocumentoNotificacao',
		'Id do tipo processo documento = Notificação',
		'69',
		true,
		true
	WHERE NOT EXISTS (SELECT 1 FROM core.tb_parametro WHERE nm_variavel = 'idTipoProcessoDocumentoNotificacao');

INSERT INTO core.tb_parametro (nm_variavel,ds_variavel,vl_variavel,in_sistema,in_ativo)
	SELECT 
		'idTipoProcessoDocumentoVistaManifestacao',
		'Id do tipo processo documento = Vista para manifestação',
		'',
		true,
		true
	WHERE NOT EXISTS (SELECT 1 FROM core.tb_parametro WHERE nm_variavel = 'idTipoProcessoDocumentoVistaManifestacao');

INSERT INTO core.tb_parametro (nm_variavel,ds_variavel,vl_variavel,in_sistema,in_ativo)
	SELECT 
		'idTipoProcessoDocumentoUrgente',
		'Id do tipo processo documento = Urgente',
		'',
		true,
		true
	WHERE NOT EXISTS (SELECT 1 FROM core.tb_parametro WHERE nm_variavel = 'idTipoProcessoDocumentoUrgente');

INSERT INTO core.tb_parametro (nm_variavel,ds_variavel,vl_variavel,in_sistema,in_ativo)
	SELECT 
		'idTipoProcessoDocumentoPautaAudienciaOuJulgamento',
		'Id do tipo processo documento = Pauta de audiência/julgamento',
		'',
		true,
		true
	WHERE NOT EXISTS (SELECT 1 FROM core.tb_parametro WHERE nm_variavel = 'idTipoProcessoDocumentoPautaAudienciaOuJulgamento');

/*
 * Alexander da Costa Monteiro
 * 22/10/2014
 * Script de inclusão do papel de Administrador de Procuradorias e Procuradores
 */

INSERT INTO acl.tb_papel(id_papel, in_condicional, ds_identificador, ds_nome, in_ativo)
SELECT nextval('acl.sq_tb_papel'), false, 'pje:papel:AdministrarProcuradoriasProcuradores', 'Administrador de Procuradorias e Procuradores', true 
WHERE NOT EXISTS (SELECT 1 FROM acl.tb_papel WHERE ds_identificador = 'pje:papel:AdministrarProcuradoriasProcuradores');

/*
 * Teofilo Emilio Soeiro dos Santos
 * PJEII-18787
 * 11/11/2014
 */

CREATE OR REPLACE FUNCTION create_index_if_not_exists (s_name text, t_name text, i_name text, index_sql text) RETURNS void AS $$
DECLARE
  full_index_name varchar;
  schema_name varchar;
  table_name varchar;
BEGIN

full_index_name = i_name;
schema_name = s_name;
table_name = t_name;

IF NOT EXISTS (
    SELECT 1
    FROM   pg_class c
    JOIN   pg_namespace n ON n.oid = c.relnamespace
    WHERE  c.relname = full_index_name
    AND    n.nspname = schema_name
    ) THEN

    execute 'CREATE INDEX ' || full_index_name || ' ON ' || schema_name || '.' || t_name || ' USING btree ' || index_sql;
END IF;
END
$$
LANGUAGE plpgsql VOLATILE;

-- SELECT create_index_if_not_exists('schema', 'table', 'index_name', '(column)');

SELECT create_index_if_not_exists('client', 'tb_pess_doc_identificacao', 'idx_tb_pessoa_doc_identificacao4', '(cd_tp_documento_identificacao COLLATE pg_catalog."default", nr_documento_identificacao COLLATE pg_catalog."default", id_pessoa)');

SELECT create_index_if_not_exists('client', 'tb_proc_doc_expediente', 'idx_tb_processo_documento_expediente3', '(id_processo_expediente, id_processo_documento, id_processo_documento_ato)');

SELECT create_index_if_not_exists('client', 'tb_proc_parte_expediente', 'idx_tb_processo_parte_expediente5', '(qt_prazo_legal_parte, ds_pendencia COLLATE pg_catalog."default", dt_prazo_legal_parte, id_pessoa_parte, id_processo_parte_expediente)');

SELECT create_index_if_not_exists('client', 'tb_proc_parte_expediente', 'idx_tb_processo_parte_expediente6', '(id_resposta, dt_ciencia_parte, in_fechado, id_pessoa_parte, id_processo_trf)');

SELECT create_index_if_not_exists('client', 'tb_processo_expediente', 'idx_tb_processo_expediente2', '(id_processo_expediente, id_proc_documento_vinculado)');

SELECT create_index_if_not_exists('client', 'tb_processo_trf', 'idx_tb_processo_trf31', '(id_processo_trf, cd_processo_status COLLATE pg_catalog."default")');

SELECT create_index_if_not_exists('client', 'tb_processo_trf', 'idx_tb_processo_trf32', '(id_processo_trf, cd_processo_status COLLATE pg_catalog."default", id_orgao_julgador)');

SELECT create_index_if_not_exists('core', 'tb_processo_instance', 'tb_processo_instance_in_ativo_idx', '(in_ativo)');

SELECT create_index_if_not_exists('public', 'jbpm_variableinstance', 'jbpm_variableinstance_longvalue_idx2', '(longvalue_,name_,class_)');

SELECT create_index_if_not_exists('public', 'jbpm_taskinstance', 'idx_taskinst_procinst_tsk_idx', '(procinst_,task_,id_)');

--Eliminando a Funcao
DROP FUNCTION create_index_if_not_exists (s_name text, t_name text, i_name text, index_sql text);

/**
* PJEII-18215 - [TJDFT] Exibir alerta para documentos não lidos do processo 
* Flávio Henrique - 29/9/2014
**/

INSERT INTO core.tb_parametro (nm_variavel,ds_variavel,vl_variavel,in_sistema,in_ativo)
SELECT 'pje:assinador:mensagemDocsNaoLidosAntesAssinatura','Mensagem de confirmação a ser exibida antes da assinatura, caso o processo possua documentos não lidos. Ver parâmetro pje:assinador:avisarAntesSobreDocsNaoLidos.','Processo com documentos não lidos. Deseja continuar?',false,true
WHERE NOT EXISTS (SELECT 1 FROM core.tb_parametro WHERE nm_variavel = 'pje:assinador:mensagemDocsNaoLidosAntesAssinatura');

INSERT INTO core.tb_parametro (nm_variavel,ds_variavel,vl_variavel,in_sistema,in_ativo)
SELECT 'pje:assinador:avisarAntesSobreDocsNaoLidos','Indica que, se o processo possua documentos não lidos, é para pedir confirmação antes da assinatura. Válido só para usuários internos. Ver parâmetro pje:assinador:mensagemDocsNaoLidosAntesAssinatura','false',false,true
WHERE NOT EXISTS (SELECT 1 FROM core.tb_parametro WHERE nm_variavel = 'pje:assinador:avisarAntesSobreDocsNaoLidos');

INSERT INTO core.tb_parametro (nm_variavel,ds_variavel,vl_variavel,in_sistema,in_ativo)
SELECT 'exibirAvisoDocsNaoLidos','Indica se o texto "Processo com documentos não lidos" deve ser exibido nas telas onde o processo é utilizado, caso a situação seja verdadeira.','false',false,true
WHERE NOT EXISTS (SELECT 1 FROM core.tb_parametro WHERE nm_variavel = 'exibirAvisoDocsNaoLidos');


/*
 * PJEII-10364
 * Thiago Vieira
 * 20/11/2014
 * */

do $$
  begin
    begin
      alter table client.tb_proc_parte_expediente add column id_procuradoria integer;
    exception
      when duplicate_column then raise notice 'coluna id_procuradoria já existe na tabela tb_proc_parte_expediente.';
    end;
  end;
$$;

update client.tb_proc_parte_expediente pa
set id_procuradoria = ppe.id_procuradoria
from client.tb_pess_prcrdoria_entidade ppe
where pa.id_pessoa_parte = ppe.id_pessoa;

do $$
  begin
    begin
      alter table client.tb_processo_parte add column id_procuradoria integer;
    exception
      when duplicate_column then raise notice 'coluna id_procuradoria já existe na tabela tb_processo_parte.';
    end;
  end;
$$;

update client.tb_processo_parte pa
		set id_procuradoria = ppe.id_procuradoria
		from client.tb_pess_prcrdoria_entidade ppe
		where pa.id_pessoa = ppe.id_pessoa;

do $$
  begin
    begin
      alter table client.tb_procuradoria add column in_tipo_procuradoria character(1) NOT NULL DEFAULT 'P';
    exception
      when duplicate_column then raise notice 'coluna in_tipo_procuradoria já existe na tabela tb_procuradoria.';
    end;
  end;
$$;

DO $$

BEGIN
	IF NOT EXISTS (SELECT 1 FROM information_schema.sequences WHERE sequence_schema = 'client' AND sequence_name = 'sq_tb_pessoa_procuradoria') THEN
		CREATE SEQUENCE client.sq_tb_pessoa_procuradoria INCREMENT 1 MINVALUE 1 START 1 CACHE 1;
	END IF;

	CREATE TABLE if not exists client.tb_pessoa_procuradoria
(
  id_pessoa_procuradoria client.id NOT NULL DEFAULT nextval('client.sq_tb_pessoa_procuradoria'::regclass),
  id_pessoa client.id not null,
  id_procuradoria client.id not null,
  in_chefe_procuradoria boolean default false,
  CONSTRAINT tb_pessoa_procuradoria_pkey PRIMARY KEY (id_pessoa_procuradoria),
  CONSTRAINT tb_pessoa_procuradoria_id_pessoa_fkey FOREIGN KEY (id_pessoa)
      REFERENCES client.tb_pessoa_procurador (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT tb_pessoa_procuradoria_id_procuradoria_fkey FOREIGN KEY (id_procuradoria)
      REFERENCES client.tb_procuradoria (id_procuradoria) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

insert into client.tb_pessoa_procuradoria(id_pessoa,id_procuradoria,in_chefe_procuradoria)
select id,id_procuradoria,in_chefe_procuradoria from client.tb_pessoa_procurador where id_procuradoria is not null;


END $$;

/*
 * Alexander da Costa Monteiro
 * Data: 22/10/2014
 * PJEII-18008
 * Script de inserção do papel de Administrador de Procuradoria
 */

insert into acl.tb_papel(id_papel, in_condicional, ds_identificador, ds_nome, in_ativo)
values(nextval('acl.sq_tb_papel'), false, 'pje:papel:administrarProcuradorias', 'Administrador de Procuradoria', true);

/**
 * Rodrigo Santos Menezes
 * [PJEII-18813]
 */

DO $$

BEGIN
	IF NOT EXISTS (SELECT 1 FROM information_schema.sequences WHERE sequence_schema = 'client' AND sequence_name = 'sq_tb_pess_proc_jurisdicao') THEN
		CREATE SEQUENCE client.sq_tb_pess_proc_jurisdicao INCREMENT 1 MINVALUE 1 START 1 CACHE 1;
	END IF;

	CREATE TABLE if not exists client.tb_pess_proc_jurisdicao
(
  id_pess_proc_jurisdicao client.id NOT NULL DEFAULT nextval('client.sq_tb_pess_proc_jurisdicao'::regclass),
  id_jurisdicao client.id not null,
  id_pessoa_procuradoria client.id not null,
  in_ativo boolean default true,
  CONSTRAINT tb_pess_proc_jurisdicao_pkey PRIMARY KEY (id_pess_proc_jurisdicao),
  CONSTRAINT tb_pess_proc_jurisdicao_id_jurisdicao_fkey FOREIGN KEY (id_jurisdicao)
      REFERENCES client.tb_jurisdicao (id_jurisdicao) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT tb_pess_proc_jurisdicao_id_pessoa_procuradoria_fkey FOREIGN KEY (id_pessoa_procuradoria)
      REFERENCES client.tb_pessoa_procuradoria (id_pessoa_procuradoria) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

END $$;

/**
 * Rodrigo Santos Menezes
 * [PJEII-18825]
 */

DO $$

BEGIN
	IF NOT EXISTS (SELECT 1 FROM information_schema.sequences WHERE sequence_schema = 'client' AND sequence_name = 'sq_tb_caixa_representante') THEN
		CREATE SEQUENCE client.sq_tb_caixa_representante INCREMENT 1 MINVALUE 1 START 1 CACHE 1;
	END IF;

	CREATE TABLE if not exists client.tb_caixa_representante
(
  id_caixa_representante client.id NOT NULL DEFAULT nextval('client.sq_tb_caixa_representante'::regclass),
  id_pessoa_fisica client.id not null,
  id_caixa_adv_proc client.id not null,
  CONSTRAINT tb_caixa_representante_pkey PRIMARY KEY (id_caixa_representante),
  CONSTRAINT tb_pessoa_fisica_id_pessoa_fisica_fkey FOREIGN KEY (id_pessoa_fisica)
      REFERENCES client.tb_pessoa_fisica (id_pessoa_fisica) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT tb_caixa_adv_proc_id_caixa_adv_proc_fkey FOREIGN KEY (id_caixa_adv_proc)
      REFERENCES client.tb_caixa_adv_proc (id_caixa_adv_proc) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

END $$;

/**
 * Rodrigo Santos Menezes
 * [PJEII-18825]
 */

DO $$

BEGIN
	IF NOT EXISTS (SELECT 1 FROM information_schema.sequences WHERE sequence_schema = 'client' AND sequence_name = 'sq_tb_periodo_inativ_caixa_rep') THEN
		CREATE SEQUENCE client.sq_tb_periodo_inativ_caixa_rep INCREMENT 1 MINVALUE 1 START 1 CACHE 1;
	END IF;

	CREATE TABLE if not exists client.tb_periodo_inativ_caixa_rep
(
  id_periodo_inativ_caixa_rep client.id NOT NULL DEFAULT nextval('client.sq_tb_periodo_inativ_caixa_rep'::regclass),
  id_caixa_adv_proc client.id not null,
  dt_inicio client.data_hora,
  dt_fim client.data_hora,
  CONSTRAINT tb_periodo_inativ_caixa_rep_pkey PRIMARY KEY (id_periodo_inativ_caixa_rep),
  CONSTRAINT tb_caixa_adv_proc_id_caixa_adv_proc_fkey2 FOREIGN KEY (id_caixa_adv_proc)
      REFERENCES client.tb_caixa_adv_proc (id_caixa_adv_proc) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

END $$;

/*
 * PJEII-18820
 * Criação da tabela com o log do histórico da movimentação
 */
DO $$

BEGIN
	IF NOT EXISTS (SELECT 1 FROM information_schema.sequences WHERE sequence_schema = 'client' AND sequence_name = 'sq_tb_log_hist_movimentacao') THEN
		CREATE SEQUENCE client.sq_tb_log_hist_movimentacao INCREMENT 1 MINVALUE 1 START 1 CACHE 1;
	END IF;

	CREATE TABLE if not exists client.tb_log_hist_movimentacao
	(
	  id_log_hist_moviment client.id NOT NULL DEFAULT nextval('client.sq_tb_log_hist_movimentacao'::regclass),
	  dt_log timestamp without time zone NOT NULL,
	  id_processo_trf integer NOT NULL,
	  id_usuario integer,
	  id_caixa_adv_proc integer,
	  cd_motivo_movimentacao character(1) NOT NULL,
	  ds_texto_movimentacao varchar not null,
	  CONSTRAINT tb_log_historico_moviment_pkey PRIMARY KEY (id_log_hist_moviment),
	  CONSTRAINT tb_log_historico_moviment_id_processo_trf_fkey FOREIGN KEY (id_processo_trf)
	      REFERENCES client.tb_processo_trf (id_processo_trf) MATCH SIMPLE
	      ON UPDATE NO ACTION ON DELETE NO ACTION,
  	  CONSTRAINT tb_log_historico_moviment_id_usuario_fkey FOREIGN KEY (id_usuario)
	      REFERENCES core.tb_usuario (id_usuario) MATCH SIMPLE
	      ON UPDATE NO ACTION ON DELETE NO ACTION
	)
	WITH (
	  OIDS=FALSE
	);	

END $$;

/*
 * Rodrigo Santos Menezes
 * PJEII-18813
 */

do $$
  begin
    begin
      alter table client.tb_pessoa_procuradoria add column in_acompanha_sessao boolean default true;
    exception
      when duplicate_column then raise notice 'coluna in_acompanha_sessao já existe na tabela tb_pessoa_procuradoria.';
    end;
  end;
$$;

/**
 * Ronaldo Ferreira de Almeida
 * [PJEII-18836]
 */

DO $$

BEGIN
IF NOT EXISTS
	(SELECT 1 FROM information_schema.columns	
		WHERE table_schema = 'client' 
		AND table_name = 'tb_caixa_adv_proc' 
		AND column_name = 'vl_nr_processo_intervalo')
THEN

	ALTER TABLE client.tb_caixa_adv_proc ADD COLUMN vl_nr_processo_intervalo text;
	COMMENT ON COLUMN client.tb_caixa_adv_proc.vl_nr_processo_intervalo IS 'Valores possíveis de filtros a serem aplicados no número do processo';

END IF;

END $$;


/**
 * Ronaldo Ferreira de Almeida
 * [PJEII-18836]
 */

DO $$

BEGIN
	alter table client.tb_caixa_adv_proc alter column dt_distribuicao_inicio type client.data_hora;
	alter table client.tb_caixa_adv_proc alter column dt_distribuicao_fim type client.data_hora;
	alter table client.tb_caixa_adv_proc alter column dt_ano_nasc_parte_inicio type client.data_hora;
	alter table client.tb_caixa_adv_proc alter column dt_ano_nasc_parte_fim type client.data_hora;	
END $$;

/**
 * Alexander da Costa Monteiro
 * [PJEII-18814]
 */

DO $$

BEGIN

ALTER TABLE client.tb_pess_proc_jurisdicao DROP CONSTRAINT IF EXISTS tb_pess_proc_jurisdicao_id_pessoa_procuradoria_fkey;

ALTER TABLE client.tb_pess_proc_jurisdicao
  ADD CONSTRAINT tb_pess_proc_jurisdicao_id_pessoa_procuradoria_fkey FOREIGN KEY (id_pessoa_procuradoria)
      REFERENCES client.tb_pessoa_procuradoria (id_pessoa_procuradoria) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE;
      
END $$;

/**
 * Rodrigo Santos Menezes
 * [PJEII-18813] Criação de tabela associativa entre ProcessoParteExpediente e CaixaAdvogadoProcurador
 */

DO $$

BEGIN
	IF NOT EXISTS (SELECT 1 FROM information_schema.sequences WHERE sequence_schema = 'client' AND sequence_name = 'sq_tb_proc_parte_exp_caixa_adv_proc') THEN
		CREATE SEQUENCE client.sq_tb_proc_parte_exp_caixa_adv_proc INCREMENT 1 MINVALUE 1 START 1 CACHE 1;
	END IF;
	
	CREATE TABLE client.tb_proc_parte_exp_caixa_adv_proc
	(
	  id_proc_parte_exp_caixa_adv_proc integer NOT NULL DEFAULT nextval('client.sq_tb_proc_parte_exp_caixa_adv_proc'::regclass),
	  id_processo_parte_expediente integer NOT NULL,
	  id_caixa_adv_proc integer NOT NULL,
	  CONSTRAINT tb_proc_parte_exp_caixa_adv_proc_pkey PRIMARY KEY (id_proc_parte_exp_caixa_adv_proc),
	  CONSTRAINT tb_ppe_caixa_adv_proc_id_caixa_adv_proc_fkey FOREIGN KEY (id_caixa_adv_proc)
	      REFERENCES client.tb_caixa_adv_proc (id_caixa_adv_proc) MATCH SIMPLE
	      ON UPDATE NO ACTION ON DELETE NO ACTION,
	  CONSTRAINT tb_ppe_caixa_adv_proc_id_processo_parte_expediente_fkey FOREIGN KEY (id_processo_parte_expediente)
	      REFERENCES client.tb_proc_parte_expediente (id_processo_parte_expediente) MATCH SIMPLE
	      ON UPDATE NO ACTION ON DELETE NO ACTION
	)
WITH (
  OIDS=FALSE
);

END $$;	

/**
 * Rodrigo Santos Menezes
 * [PJEII-18813]
 */

DO $$

BEGIN
IF NOT EXISTS
	(SELECT 1 FROM information_schema.columns	
		WHERE table_schema = 'client' 
		AND table_name = 'tb_log_hist_movimentacao' 
		AND column_name = 'id_processo_parte_expediente')
THEN

	ALTER TABLE client.tb_log_hist_movimentacao ADD COLUMN id_processo_parte_expediente integer;
	ALTER TABLE client.tb_log_hist_movimentacao ADD CONSTRAINT tb_log_hist_mov_id_processo_parte_expediente_fkey FOREIGN KEY (id_processo_parte_expediente)
    REFERENCES client.tb_proc_parte_expediente (id_processo_parte_expediente) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;
	COMMENT ON COLUMN client.tb_log_hist_movimentacao.id_processo_parte_expediente IS 'Identificador do processo parte expediente';

END IF;

END $$;

/**
 * Rodrigo Santos Menezes
 * [PJEII-18813]
 */

DO $$

BEGIN
IF NOT EXISTS
	(SELECT 1 FROM information_schema.columns	
		WHERE table_schema = 'client' 
		AND table_name = 'tb_log_hist_movimentacao' 
		AND column_name = 'nm_caixa_adv_proc')
THEN

	ALTER TABLE client.tb_log_hist_movimentacao ADD COLUMN nm_caixa_adv_proc client.varchar100;

END IF;

END $$;

/**
 * Thiago de Andrade Vieira
 * [PJEII-19261]
 */

update core.tb_processo_documento pd set ds_nome_usuario_alteracao = (select ds_nome from acl.tb_usuario_login where id_usuario = pd.id_usuario_alteracao)
where dt_juntada is not null and id_usuario_alteracao is not null and ds_nome_usuario_alteracao is null;
 
update core.tb_processo_documento pd set ds_nome_usuario_inclusao = (select ds_nome from acl.tb_usuario_login where id_usuario = pd.id_usuario_inclusao)
where dt_juntada is not null and id_usuario_inclusao is not null and ds_nome_usuario_inclusao is null;

/**
 * Thiago de Andrade Vieira
 * [PJEII-19261]
 */

CREATE OR REPLACE VIEW client.vs_destinatario_expediente AS
(
	SELECT ppe.id_processo_parte_expediente, ppe.id_pessoa_parte as id_destinatario
	FROM client.tb_proc_parte_expediente ppe
	INNER JOIN client.tb_processo_expediente pe ON ppe.id_processo_expediente = pe.id_processo_expediente
	UNION ALL
	SELECT ppe.id_processo_parte_expediente, ppr.id_representante as id_destinatario
	FROM client.tb_proc_parte_expediente ppe
	INNER JOIN client.tb_processo_trf prt ON ppe.id_processo_trf = prt.id_processo_trf
	INNER JOIN client.tb_processo_parte pp ON prt.id_processo_trf = pp.id_processo_trf and pp.id_pessoa = ppe.id_pessoa_parte
	INNER JOIN client.tb_processo_expediente pe ON ppe.id_processo_expediente = pe.id_processo_expediente
	INNER JOIN client.tb_proc_parte_represntante ppr ON pp.id_processo_parte = ppr.id_processo_parte
);


/**
 * Augusto Martins
 * Criação da funcionalidade Defensoria e atribuição de acesso aos mesmos perfis que acessam
 * a tela de Procuradoria
 */

INSERT INTO acl.tb_papel( id_papel, in_condicional, ds_identificador, ds_nome, in_ativo)
VALUES (nextval('acl.sq_tb_papel'), false, '/pages/Defensoria/listView.seam', 'Defensoria', true);

INSERT INTO acl.tb_papel_grupo (id_papel, membro_do_grupo)
SELECT id_papel, currval('acl.sq_tb_papel')
FROM acl.tb_papel_grupo p
WHERE p.membro_do_grupo = (select id_papel from acl.tb_papel where ds_identificador = '/pages/Procuradoria/listView.seam');

/**
 * Thiago Vieira
 * Tipificação automática das defensorias
  */

UPDATE client.tb_procuradoria SET in_tipo_procuradoria = 'D' where ds_nome ilike '%defensoria%';

CREATE INDEX tb_processo_parte_idx_proc
  ON client.tb_processo_parte
  (id_procuradoria);


CREATE INDEX tb_proc_parte_exp_idx_procur
  ON client.tb_proc_parte_expediente
  (id_procuradoria);


INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.3.1','VERSAO INICIAL','SQL','1.7.0.3/PJE_1.7.0.3_1__VERSAO_INICIAL.sql',831997023,'postgres',current_date,4,280,285,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.3.2','INCLUSAO IN INTIMA PESSOAL','SQL','1.7.0.3/PJE_1.7.0.3_2__INCLUSAO_IN_INTIMA_PESSOAL.sql',348398638,'postgres',current_date,1094,281,286,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.3.3','ADICIONAR PARAMETRO PAPEL MAGISTRADO PADRAO','SQL','1.7.0.3/PJE_1.7.0.3_3__ADICIONAR_PARAMETRO_PAPEL_MAGISTRADO_PADRAO.sql',-634991649,'postgres',current_date,276,282,287,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.3.4','ADICIONAR CAMPO PESSOA GRUPO OFICIAL JUSTICA','SQL','1.7.0.3/PJE_1.7.0.3_4__ADICIONAR_CAMPO_PESSOA_GRUPO_OFICIAL_JUSTICA.sql',-1665999466,'postgres',current_date,581,283,288,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.3.5','INCLUSAO PAPEL','SQL','1.7.0.3/PJE_1.7.0.3_5__INCLUSAO_PAPEL.sql',819016608,'postgres',current_date,54,284,290,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.3.6','ADICIONAR PARAMETRO QTDDIASALERTAEXPIRACAOCERTIFICADO ','SQL','1.7.0.3/PJE_1.7.0.3_6__ADICIONAR_PARAMETRO_QTDDIASALERTAEXPIRACAOCERTIFICADO_.sql',1191477187,'postgres',current_date,32,285,291,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.3.7','INCLUSAO TIPO DOCUMENTO NOS PARAMETROS','SQL','1.7.0.3/PJE_1.7.0.3_7__INCLUSAO_TIPO_DOCUMENTO_NOS_PARAMETROS.sql',-1239541964,'postgres',current_date,14,286,292,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.4.1','VERSAO INICIAL','SQL','1.7.0.4/PJE_1.7.0.4_1__VERSAO_INICIAL.sql',-71029312,'postgres',current_date,4,287,302,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.4.2','INCLUIR NOVO PAPEL','SQL','1.7.0.4/PJE_1.7.0.4_2__INCLUIR_NOVO_PAPEL.sql',2138434942,'postgres',current_date,64,288,303,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.4.3','INCLUSAO INDICES','SQL','1.7.0.4/PJE_1.7.0.4_3__INCLUSAO_INDICES.sql',-210655713,'postgres',current_date,849,289,305,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.4.4','INCLUSAO PARAMETRO AVISO DOCS NAO LIDOS','SQL','1.7.0.4/PJE_1.7.0.4_4__INCLUSAO_PARAMETRO_AVISO_DOCS_NAO_LIDOS.sql',-444552039,'postgres',current_date,137,290,306,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.1.0.1','VERSAO INICIAL','SQL','1.7.1.0/PJE_1.7.1.0_1__VERSAO_INICIAL.sql',107248464,'postgres',current_date,4,291,293,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.1.0.10','ALTERAR CAMPOS DATE PARA TIMESTAMP FILTRO CAIXA','SQL','1.7.1.0/PJE_1.7.1.0_10__ALTERAR_CAMPOS_DATE_PARA_TIMESTAMP_FILTRO_CAIXA.sql',379443864,'postgres',current_date,786,300,304,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.1.0.11','CASCADE DELETE ON PESS PROC JURISDICAO','SQL','1.7.1.0/PJE_1.7.1.0_11__CASCADE_DELETE_ON_PESS_PROC_JURISDICAO.sql',542393187,'postgres',current_date,183,301,307,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.1.0.12','CRIAR TABELA PPE CAIXA','SQL','1.7.1.0/PJE_1.7.1.0_12__CRIAR_TABELA_PPE_CAIXA.sql',-661858723,'postgres',current_date,4,302,308,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.1.0.13','CRIAR CAMPO ID PPE LOG','SQL','1.7.1.0/PJE_1.7.1.0_13__CRIAR_CAMPO_ID_PPE_LOG.sql',-692649848,'postgres',current_date,29,303,309,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.1.0.14','CAMPO NOME CAIXA LOG','SQL','1.7.1.0/PJE_1.7.1.0_14__CAMPO_NOME_CAIXA_LOG.sql',906782399,'postgres',current_date,40,304,310,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.1.0.2','PROCURADORIA','SQL','1.7.1.0/PJE_1.7.1.0_2__PROCURADORIA.sql',2075360596,'postgres',current_date,1464,292,294,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.1.0.3','INSERT IN TB PAPEL','SQL','1.7.1.0/PJE_1.7.1.0_3__INSERT_IN_TB_PAPEL.sql',-468898229,'postgres',current_date,81,293,295,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.1.0.9','CRIAR CAMPO FILTRO INTERVALO PROCESSO CAIXA PROC','SQL','1.7.1.0/PJE_1.7.1.0_9__CRIAR_CAMPO_FILTRO_INTERVALO_PROCESSO_CAIXA_PROC.sql',1781864712,'postgres',current_date,136,299,301,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.1.0.8','CRIAR CAMPO IN ACOMPANHA SESSAO','SQL','1.7.1.0/PJE_1.7.1.0_8__CRIAR_CAMPO_IN_ACOMPANHA_SESSAO.sql',783480583,'postgres',current_date,53,298,300,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.1.0.7','CREATE TABLE TB HISTORICO MOVIMENTACAO','SQL','1.7.1.0/PJE_1.7.1.0_7__CREATE_TABLE_TB_HISTORICO_MOVIMENTACAO.sql',-1682535819,'postgres',current_date,47,297,299,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.1.0.6','CRIAR TABELA PERIODO INATIVA CAIXA','SQL','1.7.1.0/PJE_1.7.1.0_6__CRIAR_TABELA_PERIODO_INATIVA_CAIXA.sql',1031816019,'postgres',current_date,23,296,298,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.1.0.5','CRIAR TABELA TB CAIXA REPRESENTANTE','SQL','1.7.1.0/PJE_1.7.1.0_5__CRIAR_TABELA_TB_CAIXA_REPRESENTANTE.sql',-334883173,'postgres',current_date,62,295,297,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.1.0.4','CRIAR TABELA TB PESS PROC JURISDICAO','SQL','1.7.1.0/PJE_1.7.1.0_4__CRIAR_TABELA_TB_PESS_PROC_JURISDICAO.sql',1660165806,'postgres',current_date,65,294,296,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.1.0.15','UPDATE PERFORMANCE PROCESSO DOCUMENTO','SQL','1.7.1.0/PJE_1.7.1.0_15__UPDATE_PERFORMANCE_PROCESSO_DOCUMENTO.sql',-884111838,'postgres',current_date,1137,305,311,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.1.0.16','VIEW PERFORMANCE EXPEDIENTES','SQL','1.7.1.0/PJE_1.7.1.0_16__VIEW_PERFORMANCE_EXPEDIENTES.sql',-1265367781,'postgres',current_date,165,306,312,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.1.0.17','INSERT FUNCIONALIDADE DEFENSORIA','SQL','1.7.1.0/PJE_1.7.1.0_17__INSERT_FUNCIONALIDADE_DEFENSORIA.sql',-842907887,'postgres','2015-01-16 15:24:47.507',4,307,313,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.1.0.18','TIPIFICACAO DEFENSORIA','SQL','1.7.1.0/PJE_1.7.1.0_18__TIPIFICACAO_DEFENSORIA.sql',2018466664,'postgres','2015-01-16 17:17:10.061',1050,308,314,true);