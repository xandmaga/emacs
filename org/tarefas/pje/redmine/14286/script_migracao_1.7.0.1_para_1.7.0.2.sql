﻿set search_path = public, acl, core, client, criminal, jt;


/*
 * Orlando Resende
 * 03/07/2014
 * Altera o tipo da colunas 'dt_inicial' e 'dt_final' para que seja possível especificar o horário do bloqueio da pauta.
 */
ALTER TABLE client.tb_bloqueio_pauta
    ALTER COLUMN dt_inicial TYPE timestamp without time zone;	
ALTER TABLE client.tb_bloqueio_pauta
    ALTER COLUMN dt_final TYPE timestamp without time zone;

/*
 * Ronaldo Ferreira de Almeida
 * 08/09/2014
 * [PJEII-18128] 
 * Aumentar o tamanho dos campos ds_texto_final_interno e ds_texto_final_externo'
 * na tabela core.tb_processo_evento para suportar 4000 characteres
 */

ALTER TABLE core.tb_processo_evento
   ALTER COLUMN ds_texto_final_interno TYPE character varying(4000);

ALTER TABLE core.tb_processo_evento
   ALTER COLUMN ds_texto_final_externo TYPE character varying(4000);

/*
 * Carlos Lisboa.
 * 13/10/2014
 * [PJEII-18082] 
 * Intimação de pauta das partes que não podem ser intimadas via sistema.
 */

INSERT INTO core.tb_parametro (nm_variavel,ds_variavel,vl_variavel,in_sistema,in_ativo)
SELECT 'pje:fluxo:prepararIntimacaoPauta','Identifica o fluxo que será iniciado com as partes que não podem ser intimadas automaticamente','PAC',true,true
WHERE NOT EXISTS (SELECT 1 FROM core.tb_parametro WHERE nm_variavel = 'pje:fluxo:prepararIntimacaoPauta');


/*
* 11/09/2014
* Criar foreign key no atributo id_documento_principal na tabela tb_processo_documento
* [PJEII-17941]
**/
--Corrigir inconcistências do banco
update core.tb_processo_documento set id_documento_principal = null 
where id_processo_documento in (select pri.id_processo_documento from core.tb_processo_documento pri
									where id_documento_principal is not null
											and not exists	(select x.id_processo_documento from core.tb_processo_documento x 
																	where x.id_processo_documento = pri.id_documento_principal));
--criando a FK
ALTER TABLE core.tb_processo_documento
  ADD CONSTRAINT tb_processo_documento_id_documento_principal_fkey FOREIGN KEY (id_documento_principal)
      REFERENCES core.tb_processo_documento (id_processo_documento) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

/**
* Thiago Machado
* Criação da sequence sq_tb_modelo_proclamacao_julgamento e da tabela tb_modelo_proclamacao_julgamento.
*/

DO $$

BEGIN
	IF NOT EXISTS (SELECT 1 FROM information_schema.sequences WHERE sequence_schema = 'client' AND sequence_name = 'sq_tb_modelo_proclamacao_julgamento') THEN
		CREATE SEQUENCE client.sq_tb_modelo_proclamacao_julgamento INCREMENT 1 MINVALUE 1 START 1 CACHE 1;
	END IF;

	CREATE TABLE IF NOT EXISTS client.tb_modelo_proclamacao_julgamento(
		id_modelo_proclamacao_julgamento client.id DEFAULT nextval('client.sq_tb_modelo_proclamacao_julgamento'::regclass) NOT NULL,
		nm_modelo VARCHAR(100) NOT NULL,
		ds_modelo text NOT NULL,
		dt_atualizacao client.data_hora NOT NULL,
		id_usuario client.id NOT NULL,
		in_ativo boolean NOT NULL,
		CONSTRAINT id_modelo_proclamacao_julgamento_pkey PRIMARY KEY (id_modelo_proclamacao_julgamento),
		CONSTRAINT nm_modelo_key UNIQUE (nm_modelo),
		CONSTRAINT id_usuario_fkey FOREIGN KEY (id_usuario)
      		REFERENCES core.tb_usuario (id_usuario)
	) WITH (
	  OIDS = FALSE
	);
	
	COMMENT ON TABLE client.tb_modelo_proclamacao_julgamento
		IS 'Tabela utilizada para armazenar os modelos de proclamação de julgamento';
END $$;

/**
 * [PJEII-17953]
 */

CREATE SEQUENCE client.sq_tb_documento_sessao
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE IF NOT EXISTS client.tb_documento_sessao 
(
  id_documento_sessao client.id NOT NULL DEFAULT nextval('client.sq_tb_documento_sessao'::regclass),
  ds_modelo_documento_sessao text, -- Descrição do modelo de documento
  id_usuario client.id, -- Identificador do usuário
  ds_signature text,
  ds_cert_chain text,
  dt_assinatura client.data_hora, -- Guarda a data e hora da primeira assinatura do processo documento bin.
  id_sessao client.id,
  CONSTRAINT id_documento_sessao PRIMARY KEY (id_documento_sessao),
  CONSTRAINT tb_documento_sessao_id_usuario_fkey FOREIGN KEY (id_usuario)
      REFERENCES core.tb_usuario (id_usuario) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT tb_documento_sessao_id_sessao_fkey FOREIGN KEY (id_sessao)
      REFERENCES client.tb_sessao (id_sessao) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)WITH (
  OIDS=FALSE
);

INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.2.1','VERSAO INICIAL','SQL','1.7.0.2/PJE_1.7.0.2_1__VERSAO_INICIAL.sql',144447377,'postgres','2014-10-07 16:30:03.662',1,272,278,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.2.2','ATUALIZA BLOQUEIO DE PAUTA PARA TIMESTAMP','SQL','1.7.0.2/PJE_1.7.0.2_2__ATUALIZA_BLOQUEIO_DE_PAUTA_PARA_TIMESTAMP.sql',1814715505,'postgres','2014-10-13 18:00:08.276',371,273,279,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.2.3','ALTERAR TAM CAMPOS TAB PROCESSO EVENTO','SQL','1.7.0.2/PJE_1.7.0.2_3__ALTERAR_TAM_CAMPOS_TAB_PROCESSO_EVENTO.sql',1300196918,'postgres','2014-10-13 18:00:08.928',36,274,280,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.2.4','ADICIONAR PARAMETRO PREPARAR INTIMACAO PAUTA','SQL','1.7.0.2/PJE_1.7.0.2_4__ADICIONAR_PARAMETRO_PREPARAR_INTIMACAO_PAUTA.sql',-1945127260,'postgres','2014-10-13 18:00:24.348',104,275,281,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.2.5','CRIAR FK NO ATRIBUTO ID DOCUMENTO PRINCIPAL NA TABELA TB PROCESSO DOCUMENTO','SQL','1.7.0.2/PJE_1.7.0.2_5__CRIAR_FK_NO_ATRIBUTO_ID_DOCUMENTO_PRINCIPAL_NA_TABELA_TB_PROCESSO_DOCUMENTO.sql',-1621089032,'postgres','2014-10-14 16:57:41.402',674,276,282,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.2.6','CREATE TABLE TB MODELO PROCLAMACAO JULGAMENTO','SQL','1.7.0.2/PJE_1.7.0.2_6__CREATE_TABLE_TB_MODELO_PROCLAMACAO_JULGAMENTO.sql',-1261852798,'postgres','2014-10-21 12:12:11.263',482,277,283,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.2.7','CRIAR TABELA DOCUMENTO SESSAO','SQL','1.7.0.2/PJE_1.7.0.2_7__CRIAR_TABELA_DOCUMENTO_SESSAO.sql',-311791401,'postgres','2014-10-21 12:12:11.978',109,278,284,true);
