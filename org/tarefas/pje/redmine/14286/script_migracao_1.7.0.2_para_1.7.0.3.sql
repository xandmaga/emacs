﻿set search_path = public, acl, core, client, criminal, jt;

/*
 * Luciano Tenylson Nogueira Costa
 * Data: 18/07/2014
 * PJEII-17397
 */
CREATE OR REPLACE FUNCTION add_in_intima_pessoal() RETURNS void AS
$BODY$
BEGIN

IF NOT EXISTS
	(SELECT 1 FROM information_schema.columns	
		WHERE table_schema = 'client' 
		AND table_name = 'tb_proc_parte_expediente' 
		AND column_name = 'in_intima_pessoal')
THEN

	ALTER TABLE client.tb_proc_parte_expediente ADD COLUMN in_intima_pessoal boolean NOT NULL DEFAULT false;
	UPDATE client.tb_proc_parte_expediente SET in_intima_pessoal = false WHERE in_intima_pessoal is null;

	

END IF;

END
$BODY$
LANGUAGE plpgsql VOLATILE STRICT;

SELECT add_in_intima_pessoal();

DROP FUNCTION add_in_intima_pessoal();

/*
 * Weberson Gabriel
 * 10/09/2014
 * PJEII-18145 - Incluir parâmetro "pje:papel:isMagistradoPadrao" para permitir indicar que o papel de magistrado sera o padrao na combo de papeis, caso o usuario logado tenha este papel 
 */
 

INSERT INTO core.tb_parametro (nm_variavel,ds_variavel,vl_variavel,in_sistema,in_ativo)
SELECT 'pje:papel:isMagistradoPadrao','Indica se o papel padrao na combo de papeis sera o Magistrado, caso o usuario logado tenha este papel (true ou false)','false',false,true
WHERE NOT EXISTS (SELECT 1 FROM core.tb_parametro WHERE nm_variavel = 'pje:papel:isMagistradoPadrao');
 
/*
* Luciano Côrtes
* 12/09/2014
* Inclusão da coluna in_ativo ta tabela tb_pess_gpo_oficial_jstica
* */

do $$
  begin
    begin
      alter table client.tb_pess_gpo_oficial_jstica add column in_ativo boolean default true;
    exception
      when duplicate_column then raise notice 'coluna in_ativo já existe na tabela tb_pess_gpo_oficial_jstica.';
    end;
  end;
$$;

/*
 * PJEII-18568
 * Rodrigo Santos Menezes
 * 10/11/2014
 * */

CREATE OR REPLACE FUNCTION executar_migracao() RETURNS void AS
$BODY$
BEGIN
IF NOT EXISTS (SELECT 1 FROM acl.tb_papel WHERE ds_identificador = 'pje:processo:visualizaPeticionamentoAvulso')
THEN
	INSERT 
		INTO acl.tb_papel 
			(id_papel, in_condicional, ds_identificador, ds_nome, in_ativo)
		VALUES
			(nextval('acl.sq_tb_papel'), false, 'pje:processo:visualizaPeticionamentoAvulso', 'Papel referente a visualizacao da aba Peticionamento Avulso',true);
END IF;
END
$BODY$
LANGUAGE plpgsql VOLATILE STRICT;

SELECT executar_migracao();
DROP FUNCTION executar_migracao();

/*
 * Thiago Vieira
 * Criação de tabela de acompanhamento de sessão de julgamento por entes externos
 * 03/10/2014
 * */

CREATE OR REPLACE FUNCTION cria_sq() RETURNS void AS
$BODY$
BEGIN
IF NOT EXISTS (SELECT 1 FROM information_schema.sequences   
		WHERE sequence_schema = 'client'
			AND sequence_name = 'sq_tb_sessao_ente_externo')
THEN

CREATE SEQUENCE client.sq_tb_sessao_ente_externo
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 342
  CACHE 1;

END IF;
END
$BODY$
LANGUAGE plpgsql VOLATILE STRICT;

SELECT cria_sq();
DROP FUNCTION cria_sq();
  
  
CREATE TABLE IF NOT EXISTS client.tb_sessao_ente_externo
(
  id_sessao_ente_externo integer NOT NULL DEFAULT nextval('client.sq_tb_sessao_ente_externo'::regclass),
  id_sessao integer NOT NULL,
  id_pessoa integer NOT NULL,
  ds_pessoa character varying(200),
  in_ativo boolean NOT NULL default true,
  
  
  CONSTRAINT tb_sessao_ente_externo_pk PRIMARY KEY (id_sessao_ente_externo),
  CONSTRAINT tb_sess_ente_ext_id_sessao_fk FOREIGN KEY (id_sessao)
      REFERENCES client.tb_sessao (id_sessao) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT tb_sess_ente_ext_id_pessoa_fk FOREIGN KEY (id_pessoa)
      REFERENCES acl.tb_usuario_login (id_usuario) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

CREATE OR REPLACE FUNCTION executar_migracao() RETURNS void AS
$BODY$
BEGIN
IF NOT EXISTS (SELECT 1 FROM pg_class t, pg_class i,pg_index ix,pg_attribute a 
							WHERE t.oid = ix.indrelid 
								AND i.oid = ix.indexrelid 
								AND a.attrelid = t.oid 
								AND a.attnum = ANY(ix.indkey) 
								AND t.relkind = 'r' 
								AND t.relname = 'tb_sessao_ente_externo' 
								AND a.attname = 'id_sessao')
THEN


CREATE INDEX idx_tb_sessao_ente_externo
  ON client.tb_sessao_ente_externo
  USING btree
  (id_sessao);
  
END IF;
IF NOT EXISTS (SELECT 1 FROM pg_class t, pg_class i,pg_index ix,pg_attribute a 
							WHERE t.oid = ix.indrelid 
								AND i.oid = ix.indexrelid 
								AND a.attrelid = t.oid 
								AND a.attnum = ANY(ix.indkey) 
								AND t.relkind = 'r' 
								AND t.relname = 'tb_sessao_ente_externo' 
								AND a.attname = 'id_pessoa')
THEN

CREATE INDEX idx_tb_sessao_ente_externo_1
  ON client.tb_sessao_ente_externo
  USING btree
  (id_pessoa);
  
END IF;
END
$BODY$
LANGUAGE plpgsql VOLATILE STRICT;

SELECT executar_migracao();
DROP FUNCTION executar_migracao();

/*
 * Adriano Pamplona
 * Data: 04/11/2014
 * Issue: [PJEII-17577]
 * 
 * Cria os parâmetros para os tipos de documento arrolados abaixo:
 * - notificação; 
 * - vista para manifestação; 
 * - urgente; e 
 * - pauta de audiência/julgamento.
 */

INSERT INTO core.tb_parametro (nm_variavel,ds_variavel,vl_variavel,in_sistema,in_ativo)
	SELECT 
		'idTipoProcessoDocumentoNotificacao',
		'Id do tipo processo documento = Notificação',
		'69',
		true,
		true
	WHERE NOT EXISTS (SELECT 1 FROM core.tb_parametro WHERE nm_variavel = 'idTipoProcessoDocumentoNotificacao');

INSERT INTO core.tb_parametro (nm_variavel,ds_variavel,vl_variavel,in_sistema,in_ativo)
	SELECT 
		'idTipoProcessoDocumentoVistaManifestacao',
		'Id do tipo processo documento = Vista para manifestação',
		'',
		true,
		true
	WHERE NOT EXISTS (SELECT 1 FROM core.tb_parametro WHERE nm_variavel = 'idTipoProcessoDocumentoVistaManifestacao');

INSERT INTO core.tb_parametro (nm_variavel,ds_variavel,vl_variavel,in_sistema,in_ativo)
	SELECT 
		'idTipoProcessoDocumentoUrgente',
		'Id do tipo processo documento = Urgente',
		'',
		true,
		true
	WHERE NOT EXISTS (SELECT 1 FROM core.tb_parametro WHERE nm_variavel = 'idTipoProcessoDocumentoUrgente');

INSERT INTO core.tb_parametro (nm_variavel,ds_variavel,vl_variavel,in_sistema,in_ativo)
	SELECT 
		'idTipoProcessoDocumentoPautaAudienciaOuJulgamento',
		'Id do tipo processo documento = Pauta de audiência/julgamento',
		'',
		true,
		true
	WHERE NOT EXISTS (SELECT 1 FROM core.tb_parametro WHERE nm_variavel = 'idTipoProcessoDocumentoPautaAudienciaOuJulgamento');

INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.3.1','VERSAO INICIAL','SQL','1.7.0.3/PJE_1.7.0.3_1__VERSAO_INICIAL.sql',831997023,'postgres','2014-11-13 18:14:32.943',1,276,294,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.3.2','INCLUSAO IN INTIMA PESSOAL','SQL','1.7.0.3/PJE_1.7.0.3_2__INCLUSAO_IN_INTIMA_PESSOAL.sql',348398638,'postgres','2014-11-13 18:14:33.004',321,277,295,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.3.3','ADICIONAR PARAMETRO PAPEL MAGISTRADO PADRAO','SQL','1.7.0.3/PJE_1.7.0.3_3__ADICIONAR_PARAMETRO_PAPEL_MAGISTRADO_PADRAO.sql',-634991649,'postgres','2014-11-13 18:14:33.366',171,278,296,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.3.4','ADICIONAR CAMPO PESSOA GRUPO OFICIAL JUSTICA','SQL','1.7.0.3/PJE_1.7.0.3_4__ADICIONAR_CAMPO_PESSOA_GRUPO_OFICIAL_JUSTICA.sql',-1665999466,'postgres','2014-11-13 18:14:33.572',119,279,297,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.3.5','INCLUSAO PAPEL','SQL','1.7.0.3/PJE_1.7.0.3_5__INCLUSAO_PAPEL.sql',819016608,'postgres','2014-11-13 18:14:33.719',115,280,298,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.3.6','CRIACAO TB SESSAO ENTE EXTERNO','SQL','1.7.0.3/PJE_1.7.0.3_6__CRIACAO_TB_SESSAO_ENTE_EXTERNO.sql',-1852172181,'postgres','2014-11-13 18:14:32.145',548,268,293,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.3.7','INCLUSAO TIPO DOCUMENTO NOS PARAMETROS','SQL','1.7.0.3/PJE_1.7.0.3_7__INCLUSAO_TIPO_DOCUMENTO_NOS_PARAMETROS.sql',-1239541964,'postgres','2015-03-05 19:30:26.184',33,282,300,true);

