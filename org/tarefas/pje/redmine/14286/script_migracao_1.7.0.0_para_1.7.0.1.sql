﻿set search_path = public, acl, core, client, criminal, jt;


/**
* Paulo André Ferreira
* 08/08/2014
* [PJEII-17847]
* Criação da sequence sq_tb_proc_parte_exp_hist e da tabela tb_proc_parte_exp_hist. 
* A tabela é utilizada como histórico para unificação e desunificação dos expedientes das pessoas envolvidas.
*/

DO $$

BEGIN
	IF NOT EXISTS (SELECT 1 FROM information_schema.sequences WHERE sequence_schema = 'client' AND sequence_name = 'sq_tb_proc_parte_exp_hist') THEN   
		CREATE SEQUENCE client.sq_tb_proc_parte_exp_hist INCREMENT 1 MINVALUE 1 START 1 CACHE 1;
	END IF;

	CREATE TABLE IF NOT EXISTS client.tb_proc_parte_exp_hist(
		id_proc_parte_exp_historico client.id NOT NULL DEFAULT nextval('client.sq_tb_proc_parte_exp_hist'::regclass),
		id_processo_parte_expediente client.id NOT NULL,
		id_pessoa_parte_origem client.id NOT NULL,
		id_usuario client.id NOT NULL,
		dt_historico client.data_hora NOT NULL,
		unificacao_ativo boolean NOT NULL,
		CONSTRAINT tb_proc_parte_exp_hist_pkey PRIMARY KEY (id_proc_parte_exp_historico),
		CONSTRAINT tb_processo_parte_expediente_fk FOREIGN KEY (id_processo_parte_expediente)
			REFERENCES client.tb_proc_parte_expediente (id_processo_parte_expediente),
		CONSTRAINT tb_proc_parte_exp_id_pessoa_parte_fkey FOREIGN KEY (id_pessoa_parte_origem)
			REFERENCES client.tb_pessoa (id_pessoa),
		CONSTRAINT tb_usuario_login_fk FOREIGN KEY (id_usuario)
			REFERENCES acl.tb_usuario_login (id_usuario)
	) WITH (
	  OIDS = FALSE
	);
	
	COMMENT ON TABLE client.tb_proc_parte_exp_hist
		IS 'Tabela utilizada como histórico para unificação e desunificação dos expedientes das pessoas envolvidas';
END $$;

/*
 * Augusto Martins
 * 19/09/2014
 * PJEII-18060 - Atualizando o campo ds_nome_usuario_exclusao em tb_processo_documento
 */
 
update core.tb_processo_documento pd
set ds_nome_usuario_exclusao = ul.ds_nome
from acl.tb_usuario_login ul 
where pd.id_usuario_exclusao = ul.id_usuario
and pd.id_usuario_exclusao is not null;

/**
* Samuel de Aguiar Rodrigues
* 25/09/2014
* [PJEII-18062]
* Adição do parâmetro conforme instrução de Rodrigo Menezes na issue.
*/

INSERT INTO core.tb_parametro(
            nm_variavel, ds_variavel, vl_variavel, dt_atualizacao,
            in_sistema, id_usuario_modificacao, in_ativo, ds_esquema_tabela_id)
    select 'IGNORA_FALHA_DE_EXECUCAO_DO_QUARTZ', 'Indicador para ignorar a execucao do Quartz quando houver falha de execucao e forcar que o quartz seja executado somente na proxima execucao agendada.', 'true', now(),
            true, null, true, null WHERE NOT EXISTS (
                SELECT 1  FROM core.tb_parametro b
                WHERE b.nm_variavel = 'IGNORA_FALHA_DE_EXECUCAO_DO_QUARTZ'
            );
			
/**
* Ronaldo Ferreira de Almeida
* 01/10/2014
* [PJEII-18223]
* Atualiza o campo de código de complemento do resultado de diligência baseado na descrição do registro.
*/

update client.tb_tp_resultado_diligencia 
   set cd_tipo_resultado_diligencia = 7
 where upper(ds_tipo_resultado_diligencia) in ('INTIMADO', 'CUMPRIDO', 'CUMPRIDO POSITIVAMENTE')
   and cd_tipo_resultado_diligencia is null;


update client.tb_tp_resultado_diligencia 
   set cd_tipo_resultado_diligencia = 8
 where upper(ds_tipo_resultado_diligencia) in ('NÃO ENCONTRADO', 'NÃO CUMPRIDO', 'CUMPRIDO NEGATIVAMENTE')
   and cd_tipo_resultado_diligencia is null;
   
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.1.1','VERSAO INICIAL','SQL','1.7.0.1/PJE_1.7.0.1_1__VERSAO_INICIAL.sql',311022201,'postgres','2014-10-03 15:08:45.7',0,258,275,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.1.2','CRIACAO TABELA PROC PARTE EXP HIST','SQL','1.7.0.1/PJE_1.7.0.1_2__CRIACAO_TABELA_PROC_PARTE_EXP_HIST.sql',-1170380299,'postgres','2014-10-03 15:08:45.741',106,259,276,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.1.3','ATUALIZAR NOME USUARIO EXCLUSAO DOCUMENTO','SQL','1.7.0.1/PJE_1.7.0.1_3__ATUALIZAR_NOME_USUARIO_EXCLUSAO_DOCUMENTO.sql',799379489,'postgres','2014-10-03 15:08:45.883',9,260,277,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.1.4','ADICAO PARAMETRO IGNORA FALHA DE EXECUCAO DO QUARTZ','SQL','1.7.0.1/PJE_1.7.0.1_4__ADICAO_PARAMETRO_IGNORA_FALHA_DE_EXECUCAO_DO_QUARTZ.sql',136124930,'postgres','2014-10-03 15:08:45.923',35,261,278,true);
INSERT INTO schema_version (version, description, type, script, checksum, installed_by, installed_on, execution_time, version_rank, installed_rank, success) VALUES ('1.7.0.1.5','ATUALIZACAO CODIGO COMPLEMENTO PARA RESULTADO DILIGENCIA','SQL','1.7.0.1/PJE_1.7.0.1_5__ATUALIZACAO_CODIGO_COMPLEMENTO_PARA_RESULTADO_DILIGENCIA.sql',-323024563,'postgres','2014-10-03 15:23:02.014',8,262,279,true);
   