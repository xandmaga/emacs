# Testes

## Descrição
Protocolar processos via MNI utilizando o MP como Ente. O ente que representa o mp é o: Ministério Público - MPMG - Pessoa Vinculada - PROCURADORIA GERAL DE JUSTIÇA DE MINAS GERAIS - CNPJ - 20971057000145

### Dados
* Usuário: 61134392672 - CARLOS ANDRE MARIANI BITTENCOURT
* Versão: 1.7.1.x
* Casos de teste

#### Casos 
Testar o protocolo com as seguintes possibilidades de polo ativo e passivo.

* Autoridade com pessoaVinculada Autoridade com pessoaVinculada
processo modelo: 5022864-76.2016.8.13.0024
classe: PROCEDIMENTO ORDINÁRIO (7)
assunto: Pessoa Idosa (11842)
ativo: Ministério Público - MPMG
passivo: ADVOCACIA GERAL DO ESTADO DE MINAS GERAIS
resultado: falha


* Xml de envio sem a tag documento para diminuir o texto do teste:

<?xml version="1.0" encoding="UTF-8"?>

<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
  <S:Body>
    <ns5:entregarManifestacaoProcessual xmlns:ns5="http://www.cnj.jus.br/servico-intercomunicacao-2.2.2/" xmlns="http://www.cnj.jus.br/tipos-servico-intercomunicacao-2.2.2" xmlns:ns2="http://www.cnj.jus.br/intercomunicacao-2.2.2" xmlns:ns4="http://www.cnj.jus.br/mni/cda" xmlns:xmime="http://www.w3.org/2005/05/xmlmime">
      <idManifestante>61134392672</idManifestante>
      <senhaManifestante>tjmg</senhaManifestante>
      <dadosBasicos classeProcessual="7" codigoLocalidade="3" competencia="11" intervencaoMP="false" nivelSigilo="0" numero="00000000000000000000">
        <ns2:polo polo="AT">
          <ns2:parte assistenciaJudiciaria="false" intimacaoPendente="0">
            <ns2:pessoa nome="Ministerio Publico - MPMG" tipoPessoa="autoridade">
              <ns2:endereco cep="36307-201">
                <ns2:logradouro>Rua Antonio Manoel de Souza Guerra</ns2:logradouro>
                <ns2:numero>277</ns2:numero>
                <ns2:bairro>Vila Marchetti</ns2:bairro>
                <ns2:cidade>SaO JOaO DEL REI</ns2:cidade>
                <ns2:estado>MG</ns2:estado>
              </ns2:endereco>
              <ns2:pessoaVinculada dataNascimento="19850613000000" nome="PROCURADORIA GERAL DE JUSTICA DE MINAS GERAIS" numeroDocumentoPrincipal="20.971.057/0001-45" tipoPessoa="juridica">
                <ns2:outroNome>miniterio publico</ns2:outroNome>
                <ns2:outroNome>MINISTERIO PUBLICO DE MINAS GERAIS</ns2:outroNome>
                <ns2:documento codigoDocumento="20.971.057/0001-45" emissorDocumento="Secretaria da Receita Federal" nome="PROCURADORIA GERAL DE JUSTICA DE MINAS GERAIS" tipoDocumento="CMF"/>
                <ns2:endereco cep="30160-030">
                  <ns2:logradouro>RUA ESPIRITO SANTO</ns2:logradouro>
                  <ns2:numero>495</ns2:numero>
                  <ns2:bairro>CENTRO</ns2:bairro>
                  <ns2:cidade>BELO HORIZONTE</ns2:cidade>
                  <ns2:estado>MG</ns2:estado>
                </ns2:endereco>
              </ns2:pessoaVinculada>
            </ns2:pessoa>
            <ns2:interessePublico>N</ns2:interessePublico>
          </ns2:parte>
        </ns2:polo>
        <ns2:polo polo="PA">
          <ns2:parte assistenciaJudiciaria="false" intimacaoPendente="0">
            <ns2:pessoa nome="ADVOCACIA GERAL DO ESTADO DE MINAS GERAIS" tipoPessoa="autoridade">
              <ns2:endereco cep="30160-030">
                <ns2:logradouro>RUA ESPIRITO SANTO</ns2:logradouro>
                <ns2:numero>495</ns2:numero>
                <ns2:bairro>CENTRO</ns2:bairro>
                <ns2:cidade>BELO HORIZONTE</ns2:cidade>
                <ns2:estado>MG</ns2:estado>
              </ns2:endereco>
              <ns2:pessoaVinculada dataNascimento="19810525000000" nome="ADVOCACIA-GERAL DO ESTADO" numeroDocumentoPrincipal="16.745.465/0001-01" tipoPessoa="juridica">
                <ns2:outroNome>estado de minas gerais</ns2:outroNome>
                <ns2:documento codigoDocumento="16.745.465/0001-01" emissorDocumento="Secretaria da Receita Federal do Brasil" nome="ADVOCACIA-GERAL DO ESTADO" tipoDocumento="CMF"/>
                <ns2:documento codigoDocumento="167454650001-01" emissorDocumento="CNPJ" nome="ESTADO DE MINAS GERAIS" tipoDocumento="RJC"/>
                <ns2:endereco cep="35500-027">
                  <ns2:logradouro>Rua Mato Grosso</ns2:logradouro>
                  <ns2:numero>600</ns2:numero>
                  <ns2:complemento>5 ANDAR</ns2:complemento>
                  <ns2:bairro>Centro</ns2:bairro>
                  <ns2:cidade>DIVINoPOLIS</ns2:cidade>
                  <ns2:estado>MG</ns2:estado>
                </ns2:endereco>
              </ns2:pessoaVinculada>
            </ns2:pessoa>
            <ns2:interessePublico>N</ns2:interessePublico>
          </ns2:parte>
        </ns2:polo>
        <ns2:assunto principal="true">
          <ns2:codigoNacional>11842</ns2:codigoNacional>
        </ns2:assunto>
        <ns2:valorCausa>2000.0</ns2:valorCausa>
      </dadosBasicos>    
      <dataEnvio>20160224183652</dataEnvio>
    </ns5:entregarManifestacaoProcessual>
  </S:Body>
</S:Envelope>

* Xml de resposta:

<?xml version="1.0" encoding="UTF-8"?>

<env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">
  <env:Header/>
  <env:Body>
    <ns1:entregarManifestacaoProcessualResposta xmlns:ns1="http://www.cnj.jus.br/servico-intercomunicacao-2.2.2/" xmlns="http://www.cnj.jus.br/tipos-servico-intercomunicacao-2.2.2" xmlns:ns2="http://www.cnj.jus.br/intercomunicacao-2.2.2" xmlns:ns3="http://www.cnj.jus.br/mni/cda">
      <sucesso>false</sucesso>
      <mensagem>Ocorreu um erro no sistema, contacte o administrador!</mensagem>
    </ns1:entregarManifestacaoProcessualResposta>
  </env:Body>
</env:Envelope>

* Exceção:

18:37:01,285 ERROR [IntercomunicacaoImpl] A entrega do processo 00000000000000000000 falhou
18:37:01,286 ERROR [IntercomunicacaoImpl]
java.lang.NullPointerException
        at br.jus.cnj.pje.intercomunicacao.servico.IntercomunicacaoService.validarDuplicidadeDePolos(IntercomunicacaoService.java:3401)
        at br.jus.cnj.pje.intercomunicacao.servico.IntercomunicacaoService.protocolarProcesso(IntercomunicacaoService.java:713)
        at br.jus.cnj.pje.intercomunicacao.servico.IntercomunicacaoService.entregarManifestacaoProcessual(IntercomunicacaoService.java:486)
        at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
        at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:57)
        at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
        at java.lang.reflect.Method.invoke(Method.java:606)
        at org.jboss.seam.util.Reflections.invoke(Reflections.java:22)
        at org.jboss.seam.intercept.RootInvocationContext.proceed(RootInvocationContext.java:32)
        at org.jboss.seam.intercept.SeamInvocationContext.proceed(SeamInvocationContext.java:56)
        at org.jboss.seam.transaction.RollbackInterceptor.aroundInvoke(RollbackInterceptor.java:28)
        at org.jboss.seam.intercept.SeamInvocationContext.proceed(SeamInvocationContext.java:68)
        at org.jboss.seam.core.BijectionInterceptor.aroundInvoke(BijectionInterceptor.java:77)
        at org.jboss.seam.intercept.SeamInvocationContext.proceed(SeamInvocationContext.java:68)
        at org.jboss.seam.bpm.BusinessProcessInterceptor.aroundInvoke(BusinessProcessInterceptor.java:51)
        at org.jboss.seam.intercept.SeamInvocationContext.proceed(SeamInvocationContext.java:68)
        at org.jboss.seam.core.MethodContextInterceptor.aroundInvoke(MethodContextInterceptor.java:44)
        at org.jboss.seam.intercept.SeamInvocationContext.proceed(SeamInvocationContext.java:68)
        at org.jboss.seam.intercept.RootInterceptor.invoke(RootInterceptor.java:107)
        at org.jboss.seam.intercept.JavaBeanInterceptor.interceptInvocation(JavaBeanInterceptor.java:185)
        at org.jboss.seam.intercept.JavaBeanInterceptor.invoke(JavaBeanInterceptor.java:103)
        at br.jus.cnj.pje.intercomunicacao.servico.IntercomunicacaoService_$$_javassist_seam_215.entregarManifestacaoProcessual(IntercomunicacaoService_$$_javassist_seam_215.java)
        at br.jus.cnj.pje.intercomunicacao.servico.IntercomunicacaoImpl.entregarManifestacaoProcessual(IntercomunicacaoImpl.java:276)
        at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
        at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:57)
        at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
        at java.lang.reflect.Method.invoke(Method.java:606)
        at org.jboss.wsf.container.jboss50.invocation.InvocationHandlerJSE.invoke(InvocationHandlerJSE.java:108)
        at org.jboss.ws.core.server.ServiceEndpointInvoker.invoke(ServiceEndpointInvoker.java:222)
        at org.jboss.wsf.stack.jbws.RequestHandlerImpl.processRequest(RequestHandlerImpl.java:474)
        at org.jboss.wsf.stack.jbws.RequestHandlerImpl.handleRequest(RequestHandlerImpl.java:295)
        at org.jboss.wsf.stack.jbws.RequestHandlerImpl.doPost(RequestHandlerImpl.java:205)
        at org.jboss.wsf.stack.jbws.RequestHandlerImpl.handleHttpRequest(RequestHandlerImpl.java:131)
        at org.jboss.wsf.common.servlet.AbstractEndpointServlet.service(AbstractEndpointServlet.java:85)
        at javax.servlet.http.HttpServlet.service(HttpServlet.java:717)
        at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:290)
        at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:206)
        at org.jboss.web.tomcat.filters.ReplyHeaderFilter.doFilter(ReplyHeaderFilter.java:96)
        at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:235)
        at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:206)
        at org.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:235)
        at org.apache.catalina.core.StandardContextValve.invoke(StandardContextValve.java:191)
        at org.jboss.web.tomcat.security.SecurityAssociationValve.invoke(SecurityAssociationValve.java:190)
        at org.apache.catalina.authenticator.AuthenticatorBase.invoke(AuthenticatorBase.java:433)
        at org.jboss.web.tomcat.security.JaccContextValve.invoke(JaccContextValve.java:92)
        at org.jboss.web.tomcat.security.SecurityContextEstablishmentValve.process(SecurityContextEstablishmentValve.java:126)
        at org.jboss.web.tomcat.security.SecurityContextEstablishmentValve.invoke(SecurityContextEstablishmentValve.java:70)
        at org.apache.catalina.core.StandardHostValve.invoke(StandardHostValve.java:127)
        at org.apache.catalina.valves.ErrorReportValve.invoke(ErrorReportValve.java:102)
        at org.jboss.web.tomcat.service.jca.CachedConnectionValve.invoke(CachedConnectionValve.java:158)
        at org.apache.catalina.core.StandardEngineValve.invoke(StandardEngineValve.java:109)
        at org.apache.catalina.connector.CoyoteAdapter.service(CoyoteAdapter.java:330)
        at org.apache.coyote.http11.Http11Processor.process(Http11Processor.java:829)
        at org.apache.coyote.http11.Http11Protocol$Http11ConnectionHandler.process(Http11Protocol.java:598)
        at org.apache.tomcat.util.net.JIoEndpoint$Worker.run(JIoEndpoint.java:447)
        at java.lang.Thread.run(Thread.java:745)
18:37:01,291 WARN  [JDBCExceptionReporter] SQL Error: 0, SQLState: null
18:37:01,291 ERROR [JDBCExceptionReporter] Transaction TransactionImple < ac, BasicAction: -3f57c7ff:44b8:56cdef78:13ff status: ActionStatus.ABORT_ONLY > cannot proceed STATUS_MARKED_ROLLBACK; - nested throwable: (javax.transaction.RollbackException: Transaction TransactionImple < ac, BasicAction: -3f57c7ff:44b8:56cdef78:13ff status: ActionStatus.ABORT_ONLY > cannot proceed STATUS_MARKED_ROLLBACK)
18:37:01,292 WARN  [JDBCExceptionReporter] SQL Error: 0, SQLState: null
18:37:01,292 ERROR [JDBCExceptionReporter] Transaction TransactionImple < ac, BasicAction: -3f57c7ff:44b8:56cdef78:13ff status: ActionStatus.ABORT_ONLY > cannot proceed STATUS_MARKED_ROLLBACK; - nested throwable: (javax.transaction.RollbackException: Transaction TransactionImple < ac, BasicAction: -3f57c7ff:44b8:56cdef78:13ff status: ActionStatus.ABORT_ONLY > cannot proceed STATUS_MARKED_ROLLBACK)
18:37:01,293 WARN  [IntercomunicacaoImpl] javax.persistence.PersistenceException: org.hibernate.exception.GenericJDBCException: could not inspect JDBC autocommit mode
18:37:01,294 INFO  [CachedConnectionManager] Closing a connection for you.  Please close them yourself: org.jboss.resource.adapter.jdbc.jdk6.WrappedConnectionJDK6@41a2d90
java.lang.Throwable: STACKTRACE
        at org.jboss.resource.connectionmanager.CachedConnectionManager.registerConnection(CachedConnectionManager.java:278)
        at org.jboss.resource.connectionmanager.BaseConnectionManager2.allocateConnection(BaseConnectionManager2.java:524)
        at org.jboss.resource.connectionmanager.BaseConnectionManager2$ConnectionManagerProxy.allocateConnection(BaseConnectionManager2.java:941)
        at org.jboss.resource.adapter.jdbc.WrapperDataSource.getConnection(WrapperDataSource.java:89)
        at org.hibernate.ejb.connection.InjectedDataSourceConnectionProvider.getConnection(InjectedDataSourceConnectionProvider.java:46)
        at org.hibernate.jdbc.ConnectionManager.openConnection(ConnectionManager.java:446)
        at org.hibernate.jdbc.ConnectionManager.getConnection(ConnectionManager.java:167)
        at org.hibernate.jdbc.AbstractBatcher.prepareQueryStatement(AbstractBatcher.java:161)
        at org.hibernate.loader.Loader.prepareQueryStatement(Loader.java:1573)
        at org.hibernate.loader.Loader.doQuery(Loader.java:696)
        at org.hibernate.loader.Loader.doQueryAndInitializeNonLazyCollections(Loader.java:259)
        at org.hibernate.loader.Loader.doList(Loader.java:2228)
        at org.hibernate.loader.Loader.listIgnoreQueryCache(Loader.java:2125)
        at org.hibernate.loader.Loader.list(Loader.java:2120)
        at org.hibernate.loader.hql.QueryLoader.list(QueryLoader.java:401)
        at org.hibernate.hql.ast.QueryTranslatorImpl.list(QueryTranslatorImpl.java:361)
        at org.hibernate.engine.query.HQLQueryPlan.performList(HQLQueryPlan.java:196)
        at org.hibernate.impl.SessionImpl.list(SessionImpl.java:1148)
        at org.hibernate.impl.QueryImpl.list(QueryImpl.java:102)
        at org.hibernate.ejb.QueryImpl.getResultList(QueryImpl.java:67)
        at br.com.itx.util.EntityUtil.getSingleResult(EntityUtil.java:398)
        at br.com.infox.cliente.util.ParametroUtil.getTipoProcessoDocumentoPeticaoInicial(ParametroUtil.java:1620)
        at br.jus.cnj.pje.intercomunicacao.servico.IntercomunicacaoService.validarExistenciaPeticaoInicial(IntercomunicacaoService.java:2964)
        at br.jus.cnj.pje.intercomunicacao.servico.IntercomunicacaoService.entregarManifestacaoProcessual(IntercomunicacaoService.java:485)
        at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
        at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:57)
        at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
        at java.lang.reflect.Method.invoke(Method.java:606)
        at org.jboss.seam.util.Reflections.invoke(Reflections.java:22)
        at org.jboss.seam.intercept.RootInvocationContext.proceed(RootInvocationContext.java:32)
        at org.jboss.seam.intercept.SeamInvocationContext.proceed(SeamInvocationContext.java:56)
        at org.jboss.seam.transaction.RollbackInterceptor.aroundInvoke(RollbackInterceptor.java:28)
        at org.jboss.seam.intercept.SeamInvocationContext.proceed(SeamInvocationContext.java:68)
        at org.jboss.seam.core.BijectionInterceptor.aroundInvoke(BijectionInterceptor.java:77)
        at org.jboss.seam.intercept.SeamInvocationContext.proceed(SeamInvocationContext.java:68)
        at org.jboss.seam.bpm.BusinessProcessInterceptor.aroundInvoke(BusinessProcessInterceptor.java:51)
        at org.jboss.seam.intercept.SeamInvocationContext.proceed(SeamInvocationContext.java:68)
        at org.jboss.seam.core.MethodContextInterceptor.aroundInvoke(MethodContextInterceptor.java:44)
        at org.jboss.seam.intercept.SeamInvocationContext.proceed(SeamInvocationContext.java:68)
        at org.jboss.seam.intercept.RootInterceptor.invoke(RootInterceptor.java:107)
        at org.jboss.seam.intercept.JavaBeanInterceptor.interceptInvocation(JavaBeanInterceptor.java:185)
        at org.jboss.seam.intercept.JavaBeanInterceptor.invoke(JavaBeanInterceptor.java:103)
        at br.jus.cnj.pje.intercomunicacao.servico.IntercomunicacaoService_$$_javassist_seam_215.entregarManifestacaoProcessual(IntercomunicacaoService_$$_javassist_seam_215.java)
        at br.jus.cnj.pje.intercomunicacao.servico.IntercomunicacaoImpl.entregarManifestacaoProcessual(IntercomunicacaoImpl.java:276)
        at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
        at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:57)
        at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
        at java.lang.reflect.Method.invoke(Method.java:606)
        at org.jboss.wsf.container.jboss50.invocation.InvocationHandlerJSE.invoke(InvocationHandlerJSE.java:108)
        at org.jboss.ws.core.server.ServiceEndpointInvoker.invoke(ServiceEndpointInvoker.java:222)
        at org.jboss.wsf.stack.jbws.RequestHandlerImpl.processRequest(RequestHandlerImpl.java:474)
        at org.jboss.wsf.stack.jbws.RequestHandlerImpl.handleRequest(RequestHandlerImpl.java:295)
        at org.jboss.wsf.stack.jbws.RequestHandlerImpl.doPost(RequestHandlerImpl.java:205)
        at org.jboss.wsf.stack.jbws.RequestHandlerImpl.handleHttpRequest(RequestHandlerImpl.java:131)
        at org.jboss.wsf.common.servlet.AbstractEndpointServlet.service(AbstractEndpointServlet.java:85)
        at javax.servlet.http.HttpServlet.service(HttpServlet.java:717)
        at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:290)
        at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:206)
        at org.jboss.web.tomcat.filters.ReplyHeaderFilter.doFilter(ReplyHeaderFilter.java:96)
        at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:235)
        at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:206)
        at org.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:235)
        at org.apache.catalina.core.StandardContextValve.invoke(StandardContextValve.java:191)
        at org.jboss.web.tomcat.security.SecurityAssociationValve.invoke(SecurityAssociationValve.java:190)
        at org.apache.catalina.authenticator.AuthenticatorBase.invoke(AuthenticatorBase.java:433)
        at org.jboss.web.tomcat.security.JaccContextValve.invoke(JaccContextValve.java:92)
        at org.jboss.web.tomcat.security.SecurityContextEstablishmentValve.process(SecurityContextEstablishmentValve.java:126)
        at org.jboss.web.tomcat.security.SecurityContextEstablishmentValve.invoke(SecurityContextEstablishmentValve.java:70)
        at org.apache.catalina.core.StandardHostValve.invoke(StandardHostValve.java:127)
        at org.apache.catalina.valves.ErrorReportValve.invoke(ErrorReportValve.java:102)
        at org.jboss.web.tomcat.service.jca.CachedConnectionValve.invoke(CachedConnectionValve.java:158)
        at org.apache.catalina.core.StandardEngineValve.invoke(StandardEngineValve.java:109)
        at org.apache.catalina.connector.CoyoteAdapter.service(CoyoteAdapter.java:330)
        at org.apache.coyote.http11.Http11Processor.process(Http11Processor.java:829)
        at org.apache.coyote.http11.Http11Protocol$Http11ConnectionHandler.process(Http11Protocol.java:598)
        at org.apache.tomcat.util.net.JIoEndpoint$Worker.run(JIoEndpoint.java:447)
        at java.lang.Thread.run(Thread.java:745)
18:37:01,299 ERROR [CachedConnectionValve] Application error: Intercomunicacao did not complete its transaction



* Autoridade com pessoaVinculada Autoridade sem pessoaVinculada
* Autoridade sem pessoaVinculada Autoridade sem pessoaVinculada
* Autoridade com pessoaVinculada Pessoa Física sem documento
* Autoridade com pessoaVinculada Pessoa Física com documento
* Autoridade com pessoaVinculada Pessoa Jurídica sem documento
* Autoridade com pessoaVinculada Pessoa Jurídica com documento 
