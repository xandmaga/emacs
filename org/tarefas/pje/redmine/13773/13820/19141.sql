﻿set search_path = public, acl, core, client, criminal, jt;

delete from core.tb_processo_tarefa_evento tpte  where id_processo in(
select id_processo from core.tb_processo_instance where id_proc_inst in(
select processinstance_ from jbpm_variableinstance vi where bytearrayvalue_ is not null
and not exists (select 1 from jbpm_byteblock where processfile_ = vi.bytearrayvalue_)))
and in_registrado = true;

delete from jbpm_variableinstance vi where bytearrayvalue_ is not null
and not exists (select 1 from jbpm_byteblock where processfile_ = vi.bytearrayvalue_);


/*select nr_processo from core.tb_processo where id_processo in(
select id_processo from core.tb_processo_instance where id_proc_inst in(
select processinstance_ from jbpm_variableinstance vi where bytearrayvalue_ is not null
and not exists (select 1 from jbpm_byteblock where processfile_ = vi.bytearrayvalue_)));*/
