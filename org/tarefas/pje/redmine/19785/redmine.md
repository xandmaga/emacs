## Problemas investigados
* Tabelas do quartz
As tabelas do quartz estão com o número normal de linhas

* Carga de dados feita pela casa de software
Foi verficado com o Rafael que os locks no banco não remetiam ao usuário que a casa de software utiliza, o sunopsys.

Além disso o Rafael relatou que o problema acontece por haver um aumento repentino do número de conexões com o banco que chega a atingir o limite do número de conexões do pool de conexões do jboss. E não por haver alguma sql sendo executada em alguma tabela que de alguma maneira bloqueie seus registros e leve a acessos concorrentes gerando assim os locks.

* Análise do log
O Rafael sugeriu que fossem analisados os logs dos servidores 41, 50 e 51, pois o aumento repentino de conexões é oriunda desses servidores.

** Observações
Diferenças entre os components.xml dos nós 41, 50 e 51 e os nós externos.

 nos 41, 50 e 51
    <core:manager concurrent-request-timeout="1800000"
        conversation-id-parameter="cid" conversation-timeout="1800000"

 nos 42, 52 e 60
    <core:manager concurrent-request-timeout="600000"
        conversation-id-parameter="cid" conversation-timeout="600000"        

Exceções comuns:
* connection
 - 2016-02-22 07:58:53,924 ERROR [org.hibernate.util.JDBCExceptionReporter] (JbpmJobExecutor@10.0.100.41:1) Unable to get managed connection for 

 -- ocorrencias: 15
 -- quando: logo antes de reiniciar

** reinicia
 
 PJE_DESCANSO_DS
 - 2016-02-22 08:35:17,076 INFO  [STDOUT] (http-0.0.0.0-8080-3) javax.security.auth.login.LoginException: Não foi possível realizar a autenticação: Verifique se o cartão ou token estão conectados.
 
 -- ocorrencias: 1180
 -- frequencia: Com muita frequência após reiniciar o jboss


 - 2016-02-22 08:35:20,309 ERROR [org.hibernate.LazyInitializationException] (Metrics Collection) failed to lazily initialize a collection of role: org.jbpm.graph.def.ProcessDefinition.definitions, no session or session was closed

 -- ocorrencias: 100
 -- frequencia: De 5 em 5 min após reiniciar o jboss

- 2016-02-22 08:39:41,352 INFO  [org.hibernate.event.def.DefaultLoadEventListener] (PJE5Scheduler_Worker-6) Error performing load command
org.hibernate.exception.GenericJDBCException: could not load an entity:

  -- ocorrencias: 216
  -- frequencia: De 20 em 20 min após reiniciar o jboss


* Foi identificado que o tipo de exceção:"br.jus.cnj.pje.extensao.StorageException", ocorreu em todos os dias da semana em que o servidor de produção apresentou instabilidade e não ocorreu na semana anterior.

** Tipos de exceções ocorridas

2016-02-22 08:46:38,227 ERROR [STDERR] (http-0.0.0.0-8080-1) br.jus.cnj.pje.extensao.StorageException: org.apache.commons.httpclient.HttpException: Erro interno 500

2016-02-22 08:48:21,665 ERROR [STDERR] (http-0.0.0.0-8080-38) br.jus.cnj.pje.extensao.StorageException: java.net.SocketException: Connection reset


