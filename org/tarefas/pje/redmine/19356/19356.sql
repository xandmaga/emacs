select oj.ds_orgao_julgador,
       pc.ds_classe_judicial,
       tproc.nr_processo,
       ttp.ds_tipo_parte,
       tusu.ds_nome

     from tb_processo_expediente pe
         inner join tb_processo tproc on tproc.id_processo = pe.id_processo_trf
         INNER JOIN tb_processo_trf pr on pr.id_processo_trf = tproc.id_processo
         INNER JOIN tb_processo_parte tpp on tpp.id_processo_trf = pr.id_processo_trf
         INNER JOIN tb_usuario_login tusu on tusu.id_usuario = tpp.id_pessoa
         INNER JOIN tb_tipo_parte ttp on ttp.id_tipo_parte = tpp.id_tipo_parte
         INNER JOIN tb_orgao_julgador oj on pr.id_orgao_julgador = oj.id_orgao_julgador
         INNER JOIN tb_classe_judicial pc on pr.id_classe_judicial = pc.id_classe_judicial
     WHERE 1 = 1
         and date_trunc('day', pe.dt_criacao_expediente) = to_date('09/12/2015', 'dd/mm/yyyy')
         and tpp.in_parte_principal = TRUE
     group by
         oj.ds_orgao_julgador,
         pc.ds_classe_judicial,
         tproc.nr_processo,
         ttp.ds_tipo_parte,
         tusu.ds_nome
     order by
         oj.ds_orgao_julgador,
         pc.ds_classe_judicial,
         tproc.nr_processo
		 
		 
select oj.ds_orgao_julgador,
       pc.ds_classe_judicial,
       tproc.nr_processo,
       ttp.ds_tipo_parte,
       tusu.ds_nome

     from tb_processo_expediente pe
         inner join tb_processo tproc on tproc.id_processo = pe.id_processo_trf
         INNER JOIN tb_processo_trf pr on pr.id_processo_trf = tproc.id_processo
         INNER JOIN tb_processo_parte tpp on tpp.id_processo_trf = pr.id_processo_trf
         INNER JOIN tb_usuario_login tusu on tusu.id_usuario = tpp.id_pessoa
         INNER JOIN tb_tipo_parte ttp on ttp.id_tipo_parte = tpp.id_tipo_parte
         INNER JOIN tb_orgao_julgador oj on pr.id_orgao_julgador = oj.id_orgao_julgador
         INNER JOIN tb_classe_judicial pc on pr.id_classe_judicial = pc.id_classe_judicial
     WHERE 1 = 1
         and date_trunc('day', pe.dt_criacao_expediente) = to_date('09/12/2015', 'dd/mm/yyyy')
         and tpp.in_parte_principal = TRUE
     group by
         oj.ds_orgao_julgador,
         pc.ds_classe_judicial,
         tproc.nr_processo,
         ttp.ds_tipo_parte,
         tusu.ds_nome
     order by
         oj.ds_orgao_julgador,
         pc.ds_classe_judicial,
         tproc.nr_processo         		 