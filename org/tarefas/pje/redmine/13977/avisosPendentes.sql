SELECT DISTINCT proc_parte_exp.* 
    ,proce.nr_processo
    ,proc_assun.id_assunto_trf
    ,org_julg.id_orgao_julgador
    ,classe.id_classe_judicial
    
    FROM "client"."tb_proc_parte_expediente" AS proc_parte_exp
	
	left join "client"."tb_processo_trf" proc_trf on
	proc_trf.id_processo_trf = proc_parte_exp.id_processo_trf
	
	left join "core"."tb_processo" proce on
	proce.id_processo = proc_trf.id_processo_trf	
	
	left join client.tb_processo_parte proc_parte on
	proc_trf.id_processo_trf = proc_parte.id_processo_trf
	
	left join client.tb_classe_judicial classe on
	classe.id_classe_judicial = proc_trf.id_classe_judicial
	
	left join client.tb_orgao_julgador org_julg on
	org_julg.id_orgao_julgador  = proc_trf.id_orgao_julgador	
	
	
	left join client.tb_processo_assunto proc_assun on
	proc_assun.id_processo_trf  = proc_trf.id_processo_trf
	
	
	left join "client"."tb_proc_parte_represntante" proc_parte_rep on
	proc_parte.id_processo_parte = proc_parte_rep.id_processo_parte
	
	inner join "client"."tb_processo_expediente" proc_exped on
	proc_exped.id_processo_expediente = proc_parte_exp.id_processo_expediente	
	
	WHERE proc_parte_exp.dt_ciencia_parte IS NULL
	AND proc_parte_exp.id_resposta IS NULL 
	AND ((proc_parte_rep.id_representante = 6353) OR proc_parte.id_pessoa IN (select id_processo_parte from client.tb_processo_parte 
	where id_pessoa = 6353)) AND proc_trf.cd_processo_status = 'D' AND proc_parte.in_situacao = 'A';