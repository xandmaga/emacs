﻿SELECT 	mag.nr_matricula,
		usu.ds_nome
	FROM "client"."tb_pessoa_magistrado" as mag
	INNER JOIN "acl"."tb_usuario_login" as usu on
	usu.id_usuario = mag.id
	order by usu.ds_nome asc;