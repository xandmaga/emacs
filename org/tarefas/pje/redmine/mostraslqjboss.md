    
   Modificar os seguintes arquivos, alterar os valores dos parâmetros que já existem e adicionar os parametros que não existem.


   1) jboss-5.1.0.GA\server\node1\deploy\pje.war\WEB-INF\classes\log4j.xml

  <!-- =========================== -->
  <!-- Parametros para mostrar sql -->
  <!-- =========================== -->

  <category name="org.hibernate">
    <priority value="INFO" />
  </category>

  <!-- show SQL DML statements as they are executed -->
  <category name="org.hibernate.SQL">
    <priority value="DEBUG" />
  </category>

  <!-- show JDBC parameters -->
  <category name="org.hibernate.type">
    <priority value="TRACE" />
  </category>

  <!-- show hql query -->
  <category name="org.hibernate.hql">
    <priority value="DEBUG" />
  </category>

  <!-- show hql query ast -->
  <category name="org.hibernate.hql.ast.AST">
    <priority value="INFO" />
  </category>  

  <!-- show jdbc sql -->
  <category name="org.hibernate.jdbc">
    <priority value="DEBUG" />
  </category>  

    
    2) jboss-5.1.0.GA\server\node1\deploy\pje.war\WEB-INF\classes\hibernate.cfg.xml

  <!-- ============================ -->
  <!-- Parametros para formatar sql -->
  <!-- ============================ -->    

    <!-- logging properties --> 
    <property name="show_sql">true</property>
    <property name="format_sql">true</property>
    <property name="use_sql_comments">true</property>